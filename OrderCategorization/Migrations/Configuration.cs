﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.EF;
using OrderCategorization.DataAccess.Model;
using System.Data.Entity.Migrations;
using System.Linq;

namespace OrderCategorization.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "OrderCategorization.DataAccess.EF.DatabaseContext";
        }

        protected override void Seed(DatabaseContext dbContext)
        {
            // Shortcuts Base Data
            if (dbContext.Shortcuts.Count() == 0)
            {
                for (short i = 1; i <= typeof(PageOrOperationType).GetEnumValues().GetLength(0); i++)
                    dbContext.Shortcuts.Add(new Shortcut { PageOrOperationTypeEnum = i, ModifierKeys = 0, Key = 145 });
            }

            // Admin User With Full Permission
            if (dbContext.Users.Count() == 0)
            {
                dbContext.Users.Add(new User { Username = "admin", HashedPassword = CryptographyUtil.GenerateSaltedHashBytes("admin") });
            }

            // Permanent Entities
            if (dbContext.Units.Count() == 0)
            {
                dbContext.Units.Add(new Unit { Title = "نامشخص", StringFormat = "{0:n0}" });
            }

            dbContext.SaveChanges();
        }
    }
}
