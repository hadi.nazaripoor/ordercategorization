﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum SelectionType
    {
        NonSelected = 0,
        AllSelected = 1,
        SubSectionSelected = 2
    }

    public enum SelectionTypeImageAddress
    {

    }
}
