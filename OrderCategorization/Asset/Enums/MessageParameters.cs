﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum MessageParameters
    {
        FirstName,
        LastName,
        FullName,
        Balance,
        DebitOrCreditPaymentDate,
        NextApportionValue,
        NextApportionDate,
        RemainedLoanValue,
        Today
    }

    public enum MessageParameterTypeFA
    {
        نام_کوچک,
        نام_خانوادگی,
        نام_کامل,
        میزان_بدهی_یا_طلب,
        تاریخ_پرداخت_بدهی_یا_دریافت_طلب,
        میزان_قسط_بعدی,
        تاریخ_قسط_بعدی,
        میزان_کل_وام,
        تاریخ_امروز
    }
}
