﻿namespace OrderCategorization.Asset.Enums
{
    public enum AccountBalanceStates
    {
        Creditor = -1,
        Zero = 0,
        Debtor = +1
    }

    public enum AccountBalanceStatesFA
    {
        بستانکار = -1,
        بی_حساب = 0,
        بدهکار = +1
    }
}
