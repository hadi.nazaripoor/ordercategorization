﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum ProductAvailabilityStates
    {
        Needed = 1,
        Enough
    }

    public enum ProductAvailabilityStatesFA
    {
        مورد_نیاز = 1,
        موجود_به_اندازه_کافی
    }
}
