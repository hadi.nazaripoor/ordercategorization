﻿namespace OrderCategorization.Asset.Enums
{
    public enum LockMessages
    {
        OK = 100,
        LockNotFoundOrCorrupted = 101,
        InvalidLock = 102,
        IncorrectSafeData = 103,
        IncorrectKeys = 103,
        UnknownError = 105,
        ExpiredLock = 106
    }

    public enum LockMessagesFA
    {
        اتصال_معتبر = 100,
        قفل_پیدا_نشده_یا_خراب_است = 101,
        قفل_نامعتبر_است = 102,
        داده_های_امن_اشتباه = 103,
        کلیدهای_اشتباه = 104,
        خطای_ناشناخته = 105,
        مهلت_استفاده_از_نرم_افزار_به_پایان_رسیده_است = 106
    }
}
