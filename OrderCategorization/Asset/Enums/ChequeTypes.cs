﻿namespace OrderCategorization.Asset.Enums
{
    public enum ChequeTypes
    {
        Recievable,
        Payable,
        RecievableGuarantee,
        PayableGuarantee
    }

    public enum ChequeTypeFA
    {
        دریافتنی,
        پرداختنی,
        دریافتنی_جهت_ضمانت,
        پرداختنی_جهت_ضمانت
    }
}
