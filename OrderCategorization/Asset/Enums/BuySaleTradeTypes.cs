﻿namespace OrderCategorization.Asset.Enums
{
    public enum BuySaleTradeTypes
    {
        MajorBuy_MajorSale = 0,
        MajorBuy_MinorSale = 1,
        MinorBuy_MinorSale = 2,
        MinorBuy_MajorSale = 3
    }

    public enum BuySaleTradeTypeFA
    {
        خرید_عمده_و_فروش_عمده = 0,
        خرید_عمده_و_فروش_خرده = 1,
        خرید_خرده_و_فروش_خرده = 2,
        خرید_خرده_فروش_عمده = 3
    }
}
