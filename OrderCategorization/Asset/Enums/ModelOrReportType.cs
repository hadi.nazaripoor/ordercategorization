﻿namespace OrderCategorization.Asset.Enums
{
    public enum ModelOrReportType
    {
        // توضیحات موقت
        // این اینام برای لیست کردن انواع صفحات لیست و نیز گزارشاتی است که می خواهیم برای آنها خروجی داشته باشیم.
        None = 0,
        Account,
        Person,
        Product,
        Outgo,
        Income,
        AccountTransactionView,
        OutgoIncomeView,
        PersonTransactionView,
        ProductCardexView,
    }

    public enum ModelOrReportTypeFA
    {
        هیچ_کدام = 0,
        حساب,
        شخص,
        محصول,
        هزینه,
        درآمد,
        گزارش_گردش_حساب,
        گزارش_هزینه_و_درآمد,
        گزارش_گردش_حساب_اشخاص,
        گزارش_کاردکس_کالا,
    }
}
