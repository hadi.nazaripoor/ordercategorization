﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum ImportExportTypes
    {
        PrintExport = 1,
        WordExport,
        PDFExport,
        ExcelExport,
        ExportSettings,
        ExcelImport,
        OptionalProperties
    }
}
