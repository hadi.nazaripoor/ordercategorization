﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum NearTransactionType
    {
        DebitPayment,
        CreditRecieve,
        ApportionPayment,
    }

    public enum NearTransactionTypeFA
    {
        بدهی_شخص_که_باید_دریافت_شود,
        طلب_شخص_که_باید_پرداخت_شود,
        قسط_شخص_که_باید_دریافت_شود,
    }
}
