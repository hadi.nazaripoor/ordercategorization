﻿namespace OrderCategorization.Asset.Enums
{
    public enum DialogeResultType
    {
        OK,
        Cancel,
        Yes,
        No,
    }
}
