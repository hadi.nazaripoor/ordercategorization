﻿using OrderCategorization.Asset.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    [Flags]
    public enum PersonType
    {
        Customer = 1,
        Company = 2,
        Personnel = 4,
        User = 8,
        Marketer = 16,
        Owner = 32,
        Investor = 64
    }

    [Flags]
    [Enum(NonSelectedStringFormat = "هیچ نوع شخصی انتخاب نشده", SingleSelectedStringFormat = "{0}", MultipleSelectedStringFormat = "{0} نوع شخص انتخاب شده")]
    public enum PersonTypeFA
    {
        مشتری = 1,
        فروشنده = 2,
        پرسنل = 4,
        کاربر_نرم_افزار = 8,
        بازاریاب = 16,
        مالک = 32,
        سرمایه_گذار = 64
    }
}
