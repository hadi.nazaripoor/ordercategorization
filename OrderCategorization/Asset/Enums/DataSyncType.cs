﻿namespace OrderCategorization.Asset.Enums
{
    public enum DataSyncType
    {
        CreateUpdate,
        RealDelete
    }
}
