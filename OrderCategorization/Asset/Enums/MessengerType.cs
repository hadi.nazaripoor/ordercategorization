﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum MessengerType
    {
        Telegram = 1,
        WhatsApp = 2,
        VoiceCall = 4,
        SMS = 8,
        Email = 16
    }
}
