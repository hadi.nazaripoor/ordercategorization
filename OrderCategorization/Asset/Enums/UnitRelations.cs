﻿using System;

namespace OrderCategorization.Asset.Enums
{
    [Flags]
    public enum UnitRelations
    {
        None = 0,
        Partial = 1,
        Completive = 2
    }

    [Flags]
    public enum UnitRelationTypeFA
    {
        تک_واحده = 0,
        دو_واحده_به_صورت_جزء_و_کل = 1,
        دو_واحده_مجزا_از_هم = 2
    }
}
