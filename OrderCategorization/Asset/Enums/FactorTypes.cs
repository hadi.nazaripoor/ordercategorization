﻿namespace OrderCategorization.Asset.Enums
{
    public enum FactorTypes
    {
        Sale,
        PreSale,
        Buy,
        PreBuy
    }

    public enum FactorTypesFA
    {
        فاکتور_فروش,
        پیش_فاکتور_فروش,
        فاکتور_خرید,
        پیش_فاکتور_خرید
    }
}
