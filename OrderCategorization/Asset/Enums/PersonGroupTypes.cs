﻿namespace OrderCategorization.Asset.Enums
{
    public enum PersonGroupTypes
    {
        NoNeedToAddAnyPerson,
        AddPreviousPersons,
        AddNewPersons,
        AddAllPersons
    }

    public enum PersonGroupTypesFA
    {
        عدم_نیاز_به_افزودن,
        افزودن_اشخاص_قدیمی,
        افزودن_اشخاص_جدید,
        افزودن_تمام_اشخاص
    }
}
