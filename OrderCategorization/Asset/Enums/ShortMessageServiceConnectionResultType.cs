﻿namespace OrderCategorization.Asset.Enums
{
    public enum ShortMessageServiceOperationResultType
    {
        NoInternetConnection = -2,
        NoConnectionData = -1,
        InvalidUserPass = 0,
        Successfull = 1,
        NoCredit = 2,
        DailyLimit = 3,
        SendLimit = 4,
        InvalidNumber = 5,
        SystemIsDisable = 6,
        BadWords= 7,
        PardisMinimumReceivers=8,
        NumberIsPublic=9,
    }
}
