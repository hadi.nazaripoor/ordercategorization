﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum ResourceKeyNames
    {
        None,

        InitializationFailedDialogePageModel,

        NeedToActivateDialogePageModel,

        ProductAlreadyActivatedDialogePageModel,

        ActivationSucceededDialogePageModel,

        WriteToLockFailedDialogePageModel,

        IncorrectActivationKeyDialogePageModel,

        IncorrectUsernameAndOrPasswordDialogePageModel,

        NewPasswordAndConfirmNewPasswordIsNotSameDialogePageModel,

        PasswordChangedSuccessfullyDialogePageModel,

        NoDataForExportDialogePageModel,

        NoColumnForExportDialogePageModel,

        NoDataFoundInExcelFileToImportDialogePageModel,

        AccessDeniedDialogePageModel,

        NoInternetConnectionDialogePageModel,

        TelegramNoConnectionDataDialogePageModel,

        TelegramAccountNeedToAuthentication,

        TelegramAuthenticatedSuccessfullyDialogePageModel,

        TelegramAuthenticationFailedPageModel,

        TelegramUnknownErrorPageModel,

        CreatePdfFileFromDocxFileFailedPageModel,

        NoPayableApportionSelectedPageModel,

        CantRemoveDebtorsOrCreditorsPageModel,

        PersonHasNoDebit,

        PersonHasNoMobileNumber,

        SelectNextTransactionDate,

        FactorDetailsContainsIncorrectRows,

        FactorIsNotCleared,

        NoPropertyFoundForThisProduct,

        FactorTemplateHasIncompleteParameters,
    }
}
