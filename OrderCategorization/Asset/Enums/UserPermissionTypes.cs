﻿using System;

namespace OrderCategorization.Asset.Enums
{
    public enum UserPermissionTypes
    {
        Product = 1,
        Person,
        Buy,
        Sale,
        Account,
        AccountTransition,
        Payment,
        Recieve,
        Cheque,
        Loan,
        Outgo,
        Income,
        User
    }

    public enum UserPermissionTypeFA
    {
        محصولات = 1,
        اشخاص,
        خرید_ها,
        فروش_ها,
        حساب_ها,
        انتقال_وجه_بین_حساب_ها,
        پرداخت_وجه_به_اشخاص,
        دریافت_وجه_از_اشخاص,
        چک_ها,
        اقساط,
        هزینه_ها,
        درآمدها,
        کاربران
    }
}
