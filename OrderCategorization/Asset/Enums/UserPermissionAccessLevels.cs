﻿using System;

namespace OrderCategorization.Asset.Enums
{
    [Flags]
    public enum UserPermissionAccessLevels
    {
        NoAccess = 0,
        CanRead = 1,
        CanCreate = 2,
        CanUpdate = 4,
        CanDelete = 8
    }
}
