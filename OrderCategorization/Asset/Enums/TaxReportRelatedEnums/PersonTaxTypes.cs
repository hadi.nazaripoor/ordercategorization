﻿namespace OrderCategorization.Asset.Enums.TaxReportRelatedEnums
{
    public enum PersonTaxTypes
    {
        TaxPayerPerson = 5,
        RelatedTo81Person = 6,
        NoNeedToPaxTaxPerson = 8,
        FinalConsumerPerson = 9,
    }

    public enum PersonTaxTypesFA
    {
        مودی_مشمول_ثبت_نام_در_نظام_مالیاتی = 5,
        مشمولین_حقیقی_ماده_81 = 6,
        اشخاصی_که_مشمول_ثبت_نام_در_نظام_مالیاتی_نیستند = 8,
        مصرف_کننده_نهایی = 9,
    }
}
