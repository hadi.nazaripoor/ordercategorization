﻿namespace OrderCategorization.Asset.Enums.TaxReportRelatedEnums
{
    public enum KalaTypes
    {
        Drug = 1,
        Tobacco = 2,
        Mobile = 3,
        Appliance = 4,
        Spare = 5,
        Petro = 6,
        Jewelry = 7,
        Clothes = 8,
        Toy = 9,
        Meat = 10,
        Agriculture = 11,
        Others = 12
    }

    public enum KalaTypesFA
    {
        دارو = 1,
        دخانیات = 2,
        موبایل = 3,
        لوازم_خانگی_برقی = 4,
        قطعات_مصرفی_و_یدکی_وسایل_نقلیه = 5,
        فرآورده_ها_و_مشتقات_نفتی_و_گازی_و_پتروشیمی = 6,
        طلا_اعم_از_شمش_و_مسکوکات_و_مصنوعات_زینتی = 7,
        منسوجات_و_پوشاک = 8,
        اسباب_بازی = 9,
        دام_زنده_و_گوشت_سفید_و_قرمز = 10,
        محصولات_اساسی_کشاورزی = 11,
        سایر_کالاها = 12,
    }
}
