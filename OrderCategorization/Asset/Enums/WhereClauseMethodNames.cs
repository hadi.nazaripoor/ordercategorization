﻿namespace OrderCategorization.Asset.Enums
{
    public enum WhereClauseMethodNames
    {
        None,
        Equals,
        Contains,
        GreaterThanOrEquals,
        LessThanOrEquals,
    }
}
