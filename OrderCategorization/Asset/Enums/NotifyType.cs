﻿using OrderCategorization.Asset.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    [Flags]
    public enum NotifyType
    {
        NoNeedToNotify = 1,
        FooterNotify = 2,
        BlockNotify = 4,
        Message = 8
    }

    [Flags]
    [Enum(NonSelectedStringFormat = "هیچ نوع یادآوری انتخاب نشده", SingleSelectedStringFormat = "{0}", MultipleSelectedStringFormat = "{0} نوع یادآوری انتخاب شده")]
    public enum NotifyTypeFA
    {
        نیازی_به_یادآوری_نیست = 1,
        یادآوری_در_پایین_صفحه = 2,
        یادآوری_در_وسط_صفحه = 4,
        یادآوری_با_ارسال_پیام = 8
    }
}
