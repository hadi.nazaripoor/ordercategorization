﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum AppLifeCycles
    {
        ApplicationLoaded,
        DatabaseInitializationSucceeded,
        DatabaseInitializationFailed,
        AccessToLockFailed,
        ActivationSucceeded,
        ActivationFailed,
        LoginSucceeded,
        LoginFailed,
        InUse,
        AfterShortcutList,
    }
}
