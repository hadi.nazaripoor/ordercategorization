﻿namespace OrderCategorization.Asset.Enums
{
    public enum DataType
    {
        Integer,
        Double,
        String,
        Date,
        Image
    }

    public enum DataTypeFA
    {
        عددی_صحیح,
        عددی_اعشاری,
        رشته_ای,
        تاریخ,
        عکس
    }
}
