﻿namespace OrderCategorization.Asset.Enums
{
    public enum TransactionCauserType
    {
        Buy,
        Sale,
        PersonTransaction,
        Outgo,
        Income,
        LoanPayment,
        LoanRecieve,
        AccountTransition,
        Person,
        Account
    }

    public enum TransactionCauserTypeFA
    {
        خرید,
        فروش,
        تراکنش_مشتری,
        هزینه,
        درآمد,
        پرداخت_وام,
        دریافت_وام,
        انتقال_وجه_میان_حساب_ها,
        حساب_اولیه_شخص,
        موجودی_اولیه_حساب
    }
}
