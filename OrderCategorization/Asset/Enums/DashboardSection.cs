﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum DashboardSection
    {
        DebtorCreditor,
        Cheque,
        Note
    }

    public enum DashboardSectionFA
    {
        لیست_بدهکاران_و_بستانکاران,
        لیست_چک_ها,
        لیست_یادداشت_ها
    }
}
