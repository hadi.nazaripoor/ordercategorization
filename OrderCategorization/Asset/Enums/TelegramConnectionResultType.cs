﻿namespace OrderCategorization.Asset.Enums
{
    public enum TelegramConnectionResultType
    {
        NoInternetConnection,
        NoConnectionData,
        NotAuthenticated,
        WrongAuthenticationCode,
        AuthenticateSuccessfully,
        ConnectionFailed,
        UnknownError,
        CodeSent
    }
}
