﻿namespace OrderCategorization.Asset.Enums
{
    public enum RecievableChequeStates
    {
        Recieved = 0,
        Passed,
        Spent,
        Deactive
    }

    public enum RecievableChequeStatesFA
    {
        دریافت_شده = 0,
        پاس_شده,
        خرج_شده,
        غیرفعال,
    }
}
