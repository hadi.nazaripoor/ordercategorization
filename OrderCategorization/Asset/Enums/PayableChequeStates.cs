﻿namespace OrderCategorization.Asset.Enums
{
    public enum PayableChequeStates
    {
        JustCreated = 0,
        Paid,
        Passed,
        Retaken,
        Deactive
    }

    public enum PayableChequeStatesFA
    {
        ایجاد_شده = 0,
        صادر_شده,
        پاس_شده,
        پس_گرفته_شده,
        غیرفعال,
    }
}
