﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum WordOperationType
    {
        OpenInWord,
        CreatePdfDocument,
        Print
    }
}
