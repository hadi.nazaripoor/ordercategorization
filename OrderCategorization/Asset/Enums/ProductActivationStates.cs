﻿using System;

namespace OrderCategorization.Asset.Enums
{
    [Flags]
    public enum ProductActivationStates
    {
        TrialVersion = 1,
        NotActivated = 2,
        Activated = 4,
        ChangedProductKey = 8,
        CantCalculateProductKey = 16
    }
}
