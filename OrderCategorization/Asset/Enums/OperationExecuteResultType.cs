﻿namespace OrderCategorization.Asset.Enums
{
    public enum OperationExecuteResultType
    {
        Canceled,
        Done,
        Failed
    }
}
