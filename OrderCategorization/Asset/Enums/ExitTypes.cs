﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum ExitTypes
    {
        UnknwonReason = 1,
        NormalExitWithBackup = 2,
        NormalExitNoBackup = 3,
        Lock = 4,
    }

    public enum ExitTypeFA
    {
        خروج_به_دلایل_نامعلوم = 1,
        خروج_معمولی_همراه_با_پشتیبان_گیری = 2,
        خروج_معمولی_بدون_پشتیبان_گیری = 3,
        قفل_نرم_افزار = 4,
    }
}
