﻿namespace OrderCategorization.Asset.Enums
{
    public enum DirectoryType
    {
        FactorTemplates,
        Logs,
        PrintedLists,
        PrintedFactors
    }
}
