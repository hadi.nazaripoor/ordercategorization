﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum DialogePageTypes
    {
        Success,
        Error,
        Warning,
        Info,
    }
}
