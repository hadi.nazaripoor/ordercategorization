﻿using System;

namespace OrderCategorization.Asset.Enums
{
    [Flags]
    public enum TotalAttributeType
    {
        Title = 0,
        Count,
        Sum,
        Average,
        Max,
        Min,
        Obj
    }
}
