﻿namespace OrderCategorization.Asset.Enums
{
    public enum SearchControlType
    {
        TextBox = 1,
        ComboBox,
        ComboBoxUC,
        DatePickerUC,
        CheckBox
    }
}
