﻿namespace OrderCategorization.Asset.Enums
{
    public enum ProductSortPriorityType
    {
        SortByName,
        SortByCategory,
        SortBySale
    }
}
