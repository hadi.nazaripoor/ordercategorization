﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Enums
{
    public enum CalculatorOperators
    {
        None = 0,
        Add,
        Subtract,
        Multiply,
        Divide,
        Equal,
        Copy,
        Cut,
        Paste,
        Clear
    }
}
