﻿namespace OrderCategorization.Asset.Enums
{
    public enum TransactionFlowDirections
    {
        Bidirectional,
        InToStore,
        OutFromStore
    }
}
