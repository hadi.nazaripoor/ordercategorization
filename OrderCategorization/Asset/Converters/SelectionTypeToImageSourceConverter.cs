﻿using OrderCategorization.Asset.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class SelectionTypeToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valueAsSelectionType = (SelectionType)value;

            switch (valueAsSelectionType)
            {
                case SelectionType.NonSelected:
                    return Constants.ALL_UNSELECTED;
                case SelectionType.AllSelected:
                    return Constants.ALL_SELECTED;
                case SelectionType.SubSectionSelected:
                    return Constants.SUBSECTION_SELECTED;
                default:
                    return Constants.ALL_UNSELECTED;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
