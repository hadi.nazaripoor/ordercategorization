﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.EF;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class UnitToBuySaleTradeTypeVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var unit = UnitRepository.GetSpecial((short)value);

            if (unit != null)
                return (UnitRelations)unit.RelationTypeEnum == UnitRelations.Partial ? Visibility.Visible : Visibility.Collapsed;

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
