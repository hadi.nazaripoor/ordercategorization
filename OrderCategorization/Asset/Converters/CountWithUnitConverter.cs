﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    class CountWithUnitConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null)
            {
                try
                {
                    var countAsDouble = (double)values[0];

                    if (values[1] != null)
                    {
                        var stringFormat = values[1].ToString();
                        return string.Format(stringFormat, countAsDouble);
                    }
                }
                catch { }
            }

            return "";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            try
            {
                var valueAsDouble = double.Parse(value.ToString());
                return new object[] { valueAsDouble };
            }
            catch { }

            return new object[] { value };
        }
    }
}
