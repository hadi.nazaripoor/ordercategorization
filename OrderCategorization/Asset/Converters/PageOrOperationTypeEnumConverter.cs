﻿using OrderCategorization.Asset.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class PageOrOperationTypeEnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return NavigationInfoComposer.Compose((PageOrOperationType)(short)value).Title;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
