﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class TransactedValueToExpressionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var transactedValue = (long)value;

                if (transactedValue == 0)
                    return $@"مبلغ تراکنش صفر است";

                if (transactedValue > 0)
                    return $@"مبلغ {string.Format("{0:n0}", value)} دریافت شد";

                return $@"مبلغ {string.Format("{0:n0}", (long)value * -1)} پرداخت شد";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
