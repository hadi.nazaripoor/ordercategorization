﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    class TextLenghtToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var showIfValueIsNullOrEmpty = parameter != null ? bool.Parse(parameter.ToString()) : true;

            if ((string)value == "")
                return showIfValueIsNullOrEmpty? Visibility.Visible : Visibility.Collapsed;

            return showIfValueIsNullOrEmpty ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
