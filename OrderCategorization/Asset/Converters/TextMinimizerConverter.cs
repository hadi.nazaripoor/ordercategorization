﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    class TextMinimizerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var lenghtOfText = int.Parse(parameter.ToString());
            var text = (string)value;

            if (text != null)
            {
                if (text.Length >= lenghtOfText)
                    return $"{text.Substring(0, lenghtOfText)}...";
                else
                    return text;
            }
            else
                return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}