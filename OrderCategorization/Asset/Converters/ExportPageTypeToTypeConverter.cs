﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.Model;
using System;

namespace OrderCategorization.Asset.Converters
{
    public class ExportPageTypeToTypeConverter
    {
        public static Type GetType(short exportPageEnum)
        {
            var relatedEnum = (ModelOrReportType)exportPageEnum;

            switch (relatedEnum)
            {
                case ModelOrReportType.Account:
                    return typeof(Account);
                case ModelOrReportType.Person:
                    return typeof(Person);
                case ModelOrReportType.Product:
                    return typeof(Product);
            }

            return null;
        }

        public static short GetShortValue(string typeName)
        {
            ModelOrReportType exportPageType = ModelOrReportType.None;

            foreach (var enumValue in typeof(ModelOrReportType).GetEnumValues())
            {
                if (enumValue.ToString() == typeName)
                    exportPageType = (ModelOrReportType)enumValue;
            }

            return (short)exportPageType;
        }
    }
}
