﻿using OrderCategorization.Asset.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class DialogePageTypeToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((DialogePageTypes)value)
            {
                case DialogePageTypes.Success:
                    return Constants.SUCCESS_FOREGROUND;
                case DialogePageTypes.Error:
                    return Constants.ERROR_FOREGROUND;
                case DialogePageTypes.Warning:
                    return Constants.WARNING_FOREGROUND;
                case DialogePageTypes.Info:
                    return Constants.INFO_FOREGROUND;
                default:
                    return Constants.SUCCESS_FOREGROUND;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
