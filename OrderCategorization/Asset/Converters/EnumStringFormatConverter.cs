﻿using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.HelperModels;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    class EnumStringFormatConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var enumItems = values[0] as ObservableCollection<ItemsSourceModel>;
            if (values != null)
            {
                var selectedCount = enumItems.Count(item => item.IsSelected);

                var enumType = (Type)values[1];
                if (enumType != null)
                {
                    var attributes = enumType.GetCustomAttributes(typeof(EnumAttribute), false);
                    if (attributes != null)
                    {
                        var firstAttribute = attributes[0] as EnumAttribute;
                        if (firstAttribute != null)
                        {
                            if (selectedCount == 0)
                                return firstAttribute.NonSelectedStringFormat;
                            else if (selectedCount == 1)
                                return String.Format(firstAttribute.SingleSelectedStringFormat, enumItems.FirstOrDefault(item => item.IsSelected).DisplayMember);
                            else
                                return String.Format(firstAttribute.MultipleSelectedStringFormat, selectedCount);
                        }
                    }
                }
            }

            return "";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
