﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null)
                return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;

            return ((bool)value == bool.Parse(parameter.ToString())) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
