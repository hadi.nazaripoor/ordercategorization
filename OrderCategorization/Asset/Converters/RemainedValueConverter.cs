﻿using OrderCategorization.Asset.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class RemainedValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var mustTransactedValue = (long)values[0];
            var totalTransacted = (long)values[1];
            var transactionFlowDirection = (TransactionFlowDirections)values[2];

            var signedMustTransactedValue = mustTransactedValue * ((transactionFlowDirection == TransactionFlowDirections.InToStore) ? 1 : -1);

            if (signedMustTransactedValue == totalTransacted)
                return $"مبلغ {mustTransactedValue.ToString("n0")} به طور کامل " + (transactionFlowDirection == TransactionFlowDirections.OutFromStore ? "پرداخت شد" : "دریافت شد");

            var differenceValue = signedMustTransactedValue - totalTransacted;

            if(transactionFlowDirection == TransactionFlowDirections.InToStore)
            {
                if (differenceValue > 0) return $"از مبلغ کل {mustTransactedValue.ToString("n0")}، مبلغ { Math.Abs(differenceValue).ToString("n0")} کمتر دریافت شد.";
                else return $"از مبلغ کل {mustTransactedValue.ToString("n0")}، مبلغ { Math.Abs(differenceValue).ToString("n0")} بیشتر دریافت شد.";
            }
            else
            {
                if (differenceValue < 0) return $"از مبلغ کل {mustTransactedValue.ToString("n0")}، مبلغ { Math.Abs(differenceValue).ToString("n0")} کمتر پرداخت شد.";
                else return $"از مبلغ کل {mustTransactedValue.ToString("n0")}، مبلغ { Math.Abs(differenceValue).ToString("n0")} بیشتر پرداخت شد.";
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
