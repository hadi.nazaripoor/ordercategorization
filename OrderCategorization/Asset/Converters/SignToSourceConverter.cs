﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OrderCategorization.Asset.Converters
{
    public class SignToSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if ((int)value == 0)
            //    return @"/OrderCategorization;component/Images/CommonIcon/InToStore.png";

            if ((int)value == 1)
                return @"/OrderCategorization;component/Images/CommonIcon/InToStore.png";

                return @"/OrderCategorization;component/Images/CommonIcon/OutFromStore.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
