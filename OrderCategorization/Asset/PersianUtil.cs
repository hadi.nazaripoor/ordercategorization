﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using System;
using System.Globalization;

namespace OrderCategorization.Asset
{
    public class PersianUtil
    {
        public static string PersianDate(DateTime dateToConvert, bool isShortFormat = true)
        {
            var persianCalendar = new PersianCalendar();

            int day = persianCalendar.GetDayOfMonth(dateToConvert);
            int month = persianCalendar.GetMonth(dateToConvert);
            int year = persianCalendar.GetYear(dateToConvert);

            if (isShortFormat)
                return $"{year:0000}/{month:00}/{day:00}";

            var persinaDayOfWeek = PersianDayOfWeek(dateToConvert);
            var persianMonth = (PersianMonth)month;

            return $"{persinaDayOfWeek.ToString()}، {day} {persianMonth.ToString()} {year}";
        }

        public static string PersianDate(bool isShortFormat = true)
        {
            return PersianDate(DateTime.Today, isShortFormat);
        }

        public static CalendarInfo PersianDateAsCalendarInfo()
        {
            return PersianDateAsCalendarInfo(DateTime.Today);
        }

        public static CalendarInfo PersianDateAsCalendarInfo(DateTime today)
        {
            var simplePersianDate = PersianDate(today);
            var simplePersianDateParts = simplePersianDate.Split('/');

            return new CalendarInfo
            {
                Day = int.Parse(simplePersianDateParts[2]), // Intentional :: Day Must Set First
                Month = int.Parse(simplePersianDateParts[1]),
                Year = int.Parse(simplePersianDateParts[0]),
            };
        }

        public static PersianDayOfWeek PersianDayOfWeek(DateTime dateToConvert)
        {
            PersianCalendar persianCalendar = new PersianCalendar();

            return (PersianDayOfWeek)(((short)persianCalendar.GetDayOfWeek(dateToConvert) + 1) % 7 + 1);
        }

        public static PersianDayOfWeek PersianDayOfWeek()
        {
            return PersianDayOfWeek(DateTime.Today);
        }

        public static bool IsValidDate(string date)
        {
            try
            {
                var miladiDate = MiladiDate(date);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static DateTime MiladiDate(string persianDate)
        {
            PersianCalendar pc = new PersianCalendar();
            string[] dateParts = persianDate.Split('/');

            return pc.ToDateTime(int.Parse(dateParts[0]), int.Parse(dateParts[1]), int.Parse(dateParts[2]), 0, 0, 0, 0);
        }

        public static int DifferenceBetweenTwoDates(string firstDate, string secondDate)
        {
            if (!(string.IsNullOrWhiteSpace(firstDate) || string.IsNullOrWhiteSpace(secondDate)))
            {
                DateTime firstDateTime = MiladiDate(firstDate), secondDateTime = MiladiDate(secondDate);

                return (int)(secondDateTime - firstDateTime).TotalDays;
            }

            return Constants.DONT_CARE_VALUE;
        }

        public static bool IsLeapYear(int year)
        {
            return year % 4 == 3;
        }

        public static void SetMonthInfo(int year, int month, ref int lastDayOfPreMonth, ref int firstDayOfThisMonthDayOfWeek, ref int thisMonthDaysCount)
        {
            string persianDate = $"{year.ToString("0000")}/{month.ToString("00")}/01";
            PersianDayOfWeek dayOfWeek = PersianDayOfWeek(MiladiDate(persianDate));

            firstDayOfThisMonthDayOfWeek = (short)dayOfWeek;
            thisMonthDaysCount = (month < 7) ? 31 : 30;
            thisMonthDaysCount = !IsLeapYear(year) && (month == 12) ? 29 : thisMonthDaysCount;

            var preMonth = month - 1;
            if (preMonth == 0)
            {
                preMonth += 12;
                year--;
            }

            lastDayOfPreMonth = (preMonth < 7) ? 31 : 30;
            lastDayOfPreMonth = !IsLeapYear(year) && (preMonth == 12) ? 29 : lastDayOfPreMonth;
        }

        internal static string PersianTime()
        {
            var now = DateTime.Now;

            return $"{now.Hour.ToString("00")}:{now.Minute.ToString("00")}";
        }
    }
}
