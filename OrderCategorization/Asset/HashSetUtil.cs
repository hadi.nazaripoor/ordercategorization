﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset
{
    public class HashSetUtil
    {
        public static HashSet<TResult> GetHashSet<TInput, TResult>(List<TInput> ids)
        {
            var idsHashSet = new HashSet<TResult>();

            foreach (var id in ids)
                idsHashSet.Add((TResult)Convert.ChangeType(id, typeof(TResult)));

            return idsHashSet;
        }
    }
}
