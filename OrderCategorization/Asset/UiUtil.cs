﻿using OrderCategorization.Controls;
using OrderCategorization.DataAccess.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrderCategorization.Asset
{
    public class UiUtil
    {
        public static TextBox GetTextBox(string title)
        {
            return new TextBox { Tag = title, Height = 42 };
        }

        public static ComboBox GetComboBox(string title)
        {
            return new ComboBox { Tag = title, Height = 42 };
        }

        public static MultiSelectComboBoxUC GetMultiSelectUC(string title)
        {
            return new MultiSelectComboBoxUC { Tag = title, Height = 42 };
        }

        public static ComboBoxUC GetComboBoxUC(string title)
        {
            return new ComboBoxUC { Tag = title, MinHeight = 42 };
        }

        public static Rectangle GetSeparatorRectangle()
        {
            return new Rectangle { Stroke = Brushes.WhiteSmoke, VerticalAlignment = VerticalAlignment.Bottom, StrokeThickness = 0.5, Margin = new Thickness(24, 8, 24, 8) };
        }

        public static ImageSource GetImageSourceByAbsolutePath(string imagePath)
        {
            return new BitmapImage(new Uri(imagePath, UriKind.Absolute));
        }
    }
}
