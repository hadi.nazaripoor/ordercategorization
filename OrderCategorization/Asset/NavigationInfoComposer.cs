﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using System;

namespace OrderCategorization.Asset
{
    public class NavigationInfoComposer
    {
        public static NavigationInfo Compose(PageOrOperationType pageOrOperationEnum)
        {
            var preConstUri = "pack://application:,,,/OrderCategorization;component/";
            NavigationInfo navigationInfo = new NavigationInfo();

            switch (pageOrOperationEnum)
            {
                case PageOrOperationType.ProductList:
                    navigationInfo.Title = "محصولات";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/ProductListPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/Account.png";
                    break;
                case PageOrOperationType.Product:
                    navigationInfo.Title = "محصول جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/ProductPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/Calculator.png";
                    break;
                case PageOrOperationType.ProductFromExcel:
                    navigationInfo.Title = "افزودن گروهی محصولات";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/Category.png";
                    break;
                case PageOrOperationType.ProductCatList:
                    navigationInfo.Title = "گروه بندی محصولات";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/ProductCatListPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/Calendar.png";
                    break;
                case PageOrOperationType.UnitList:
                    navigationInfo.Title = "واحدهای اندازه گیری";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/UnitListPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/Backup.png";
                    break;
                case PageOrOperationType.NeededProduct:
                    navigationInfo.Title = "اقلام مورد نیاز";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/NeededProductListPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/Messaging.png";
                    break;
                case PageOrOperationType.PersonList:
                    navigationInfo.Title = "اشخاص";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/PersonListPage.xaml");
                    break;
                case PageOrOperationType.Person:
                    navigationInfo.Title = "شخص جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/PersonPage.xaml");
                    break;
                case PageOrOperationType.PersonFromExcel:
                    navigationInfo.Title = "افزودن گروهی اشخاص";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.PersonGroupList:
                    navigationInfo.Title = "گروه های اشخاص";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/PersonGroupListPage.xaml");
                    break;
                case PageOrOperationType.BuyList:
                    navigationInfo.Title = "فاکتورهای خرید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/BuyLogListPage.xaml");
                    break;
                case PageOrOperationType.Buy:
                    navigationInfo.Title = "خرید جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/BuyPage.xaml");
                    break;
                case PageOrOperationType.SaleList:
                    navigationInfo.Title = "فاکتورهای فروش";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/SaleLogListPage.xaml");
                    break;
                case PageOrOperationType.Sale:
                    navigationInfo.Title = "فروش جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/SalePage.xaml");
                    break;
                case PageOrOperationType.SalePreFactorList:
                    navigationInfo.Title = "پیش فاکتورهای فروش";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.SalePreFactor:
                    navigationInfo.Title = "پیش فاکتور جدید";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.FactorTemplate:
                    navigationInfo.Title = "تنظیمات قالب فاکتور";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/FactorTemplatePage.xaml");
                    break;
                case PageOrOperationType.AccountList:
                    navigationInfo.Title = "صندوق و بانک";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/AccountListPage.xaml");
                    break;
                //case PageOrOperationType.Account:
                //    navigationInfo.Title = "حساب جدید";
                //    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/AccountPage.xaml");
                //    break;
                case PageOrOperationType.AccountTransitionList:
                    navigationInfo.Title = "انتقال ها";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/AccountTransitionListPage.xaml");
                    break;
                case PageOrOperationType.AccountTransition:
                    navigationInfo.Title = "انتقال جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/AccountTransitionPage.xaml");
                    break;
                case PageOrOperationType.PersonTransactionListPage:
                    navigationInfo.Title = "تراکنش ها";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/PersonTransactionListPage.xaml");
                    break;
                case PageOrOperationType.Recieve:
                    navigationInfo.Title = "دریافت جدید";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.Payment:
                    navigationInfo.Title = "پرداخت جدید";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.PersonTransactionTemplate:
                    navigationInfo.Title = "قالب رسید دریافت و پرداخت";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.PayableChequeList:
                    navigationInfo.Title = "چک های پرداختنی";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/PayableChequeListPage.xaml");
                    break;
                case PageOrOperationType.PayableCheque:
                    navigationInfo.Title = "ثبت چک پرداختنی جدید";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.ChequeBookList:
                    navigationInfo.Title = "دفترچه های چک";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/ChequeBookListPage.xaml");
                    break;
                case PageOrOperationType.RecieveableChequeList:
                    navigationInfo.Title = "چک های دریافتنی";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.RecieveableCheque:
                    navigationInfo.Title = "چک دریافتنی جدید";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.LoanList:
                    navigationInfo.Title = "وام ها";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/LoanListPage.xaml");
                    break;
                case PageOrOperationType.Loan:
                    navigationInfo.Title = "دفترچه اقساط جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/LoanPage.xaml");
                    break;
                case PageOrOperationType.OutgoList:
                    navigationInfo.Title = "هزینه ها";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/OutgoListPage.xaml");
                    break;
                case PageOrOperationType.Outgo:
                    navigationInfo.Title = "هزینه جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/OutgoPage.xaml");
                    break;
                case PageOrOperationType.OutgoCatList:
                    navigationInfo.Title = "دسته های هزینه";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/OutgoCatListPage.xaml");
                    break;
                case PageOrOperationType.IncomeList:
                    navigationInfo.Title = "درآمدها";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/IncomeListPage.xaml");
                    break;
                case PageOrOperationType.Income:
                    navigationInfo.Title = "درآمد جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/IncomePage.xaml");
                    break;
                case PageOrOperationType.IncomeCatList:
                    navigationInfo.Title = "دسته های درآمد";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/IncomeCatListPage.xaml");
                    break;
                case PageOrOperationType.TagList:
                    navigationInfo.Title = "مراکز هزینه و درآمد";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/TagListPage.xaml");
                    break;
                case PageOrOperationType.SMSList:
                    navigationInfo.Title = "پیامک های دریافتی";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.SMS:
                    navigationInfo.Title = "ارسال پیامک جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/MessagePage.xaml");
                    break;
                case PageOrOperationType.TelegramList:
                    navigationInfo.Title = "پیام های تلگرامی ارسالی";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.Telegram:
                    navigationInfo.Title = "ارسال پیام تلگرامی جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/MessagePage.xaml");
                    break;
                case PageOrOperationType.RecievedTelegramList:
                    navigationInfo.Title = "پیام های تلگرامی دریافتی";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.RecievedCommandTemplate:
                    navigationInfo.Title = "قالب دستورات پیام های دریافتی";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.Dashboard:
                    navigationInfo.Title = "داشبورد";
                    break;
                case PageOrOperationType.ReportList:
                    navigationInfo.Title = "لیست گزارشات";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/ReportListPage.xaml");
                    break;
                case PageOrOperationType.AccountTransactionReport:
                    navigationInfo.Title = "گزارش تراکنش حساب ها";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/AccountTransactionReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Account.png";
                    break;
                case PageOrOperationType.PersonTransactionReport:
                    navigationInfo.Title = "گزارش تراکنش اشخاص";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/PersonTransactionReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Person.png";
                    break;
                case PageOrOperationType.ProductCartexReport:
                    navigationInfo.Title = "گزارش کاردکس کالا";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/ProductCardexReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Product.png";
                    break;
                case PageOrOperationType.ProductValueReport:
                    navigationInfo.Title = "گزارش ارزش ریالی کالا";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/ProductValueReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Product.png";
                    break;
                case PageOrOperationType.TaxReport:
                    navigationInfo.Title = "گزارش معاملات فصلی";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/SaleTaxReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/TTMS.png";
                    break;
                case PageOrOperationType.OutgoIncomeReport:
                    navigationInfo.Title = "گزارش هزینه ها و درآمد";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/OutgoIncomeReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/InOut.png";
                    break;
                case PageOrOperationType.LoanReport:
                    navigationInfo.Title = "گزارش دفترچه های اقساط";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/LoanReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Loan.png";
                    break;
                case PageOrOperationType.ChequeReport:
                    navigationInfo.Title = "گزارش گردش چک";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Cheque.png";
                    break;
                case PageOrOperationType.BuySaleReport:
                    navigationInfo.Title = "گزارش خرید و فروش";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/BuySale.png";
                    break;
                case PageOrOperationType.OverallReport:
                    navigationInfo.Title = "گزارش جامع";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Overall.png";
                    break;
                case PageOrOperationType.FinanceTReport:
                    navigationInfo.Title = "گزارش تراز مالی";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/Balance.png";
                    break;
                case PageOrOperationType.GainLoseReport:
                    navigationInfo.Title = "گزارش سود و زیان";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Reports/ProfitLossReportPage.xaml");
                    navigationInfo.IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/ReportIcons/ProfitLoss.png";
                    break;
                case PageOrOperationType.UserList:
                    navigationInfo.Title = "کاربران نرم افزار";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/UserListPage.xaml");
                    break;
                case PageOrOperationType.User:
                    navigationInfo.Title = "کاربر جدید";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/CUs/UserPage.xaml");
                    break;
                case PageOrOperationType.UserPermission:
                    navigationInfo.Title = "دسترسی های من";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/MyPermissionsListPage.xaml");
                    break;
                case PageOrOperationType.ChangePassword:
                    navigationInfo.Title = "تغییر کلمه عبور";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/ChangePasswordPage.xaml");
                    break;
                case PageOrOperationType.UserAccessLogList:
                    navigationInfo.Title = "سابقه ورود و خروج کاربران";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/UserAccessLogListPage.xaml");
                    break;
                case PageOrOperationType.NoteList:
                    navigationInfo.Title = "ابزار یادداشت";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/NoteListPage.xaml");
                    break;
                case PageOrOperationType.Settings:
                    navigationInfo.Title = "تنظیمات نرم افزار";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/SettingsPage.xaml");
                    break;
                case PageOrOperationType.QouteList:
                    navigationInfo.Title = "کلام بزرگان";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/QouteListPage.xaml");
                    break;
                case PageOrOperationType.Show_Calculator:
                    navigationInfo.Title = "ماشین حساب";
                    //navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/ProductCatListPage.xaml");
                    break;
                case PageOrOperationType.ShortcutList:
                    navigationInfo.Title = "کلیدهای میانبر نرم افزار";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/ShortcutListPage.xaml");
                    break;
                case PageOrOperationType.Take_Picture:
                    navigationInfo.Title = "گرفتن تصویر از نرم افزار";
                    //navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/Lists/ProductCatListPage.xaml");
                    break;
                case PageOrOperationType.Backup_Data:
                    navigationInfo.Title = "پشتیبان گیری از داده ها";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.Restore_Data:
                    navigationInfo.Title = "بازیابی داده ها";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.ProductActivation:
                    navigationInfo.Title = "فعالسازی نرم افزار";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/ProductActivationPage.xaml");
                    break;
                case PageOrOperationType.LiveHelp:
                    navigationInfo.Title = "راهنمای زنده نرم افزار";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.VideoHelp:
                    navigationInfo.Title = "راهنمای تصویری نرم افزار";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.TextHelp:
                    navigationInfo.Title = "راهنمای متنی نرم افزار";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.AboutSoftware:
                    navigationInfo.Title = "درباره نرم افزار";
                    //navigationInfo.NavigationPageSource = new Uri("");
                    break;
                case PageOrOperationType.AboutUs:
                    navigationInfo.Title = "درباره ما";
                    navigationInfo.NavigationPageSource = new Uri($"{preConstUri}Views/AboutUsPage.xaml");
                    break;
            }

            //navigationInfo.Shortcut = ShortcutRepository.GetShortcutByPageOrOperationEnum((short)pageOrOperationEnum);
            return navigationInfo;
        }
    }
}
