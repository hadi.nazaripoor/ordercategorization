﻿using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.HelperModels;
using System;
using System.Collections.ObjectModel;

namespace OrderCategorization.Asset
{
    public class EnumUtil
    {
        public static ObservableCollection<ItemsSourceModel> EnumTypeToItemsSourceCollection(Type sourceEnumType, bool hasDontCareItem = false)
        {
            var resultItemsSource = new ObservableCollection<ItemsSourceModel>();

            if (hasDontCareItem)
                resultItemsSource.Add(new ItemsSourceModel { IntegerValueMember = Constants.DONT_CARE_VALUE, DisplayMember = "بدون اهمیت" });

            foreach (var item in Enum.GetValues(sourceEnumType))
                resultItemsSource.Add(new ItemsSourceModel { IntegerValueMember = (int)item, DisplayMember = item.ToString().Replace("_", " ") });

            return resultItemsSource;
        }

        public static string FlaggedEnumValueToString(Type flaggedEnumType, int value)
        {
            var attributes = flaggedEnumType.GetCustomAttributes(typeof(EnumAttribute), false);
            if (attributes != null)
            {
                var firstAttribute = attributes[0] as EnumAttribute;
                if (firstAttribute != null)
                {
                    var result = "";
                    var valueCount = 0;

                    foreach (var enumValue in flaggedEnumType.GetEnumValues())
                    {
                        if (((int)enumValue | value) == value)
                        {
                            result += $"{enumValue.ToString().Replace("_", " ")} ";
                            valueCount++;
                        }
                    }

                    if (valueCount == 0)
                        return firstAttribute.NonSelectedStringFormat;
                    else if (valueCount == 1)
                        return String.Format(firstAttribute.SingleSelectedStringFormat, result);
                    else
                        return String.Format(firstAttribute.MultipleSelectedStringFormat, valueCount);
                }
            }

            return "";
        }

        public static ObservableCollection<ItemsSourceModel> FlaggedEnumValues(Type flaggedEnumType, int value)
        {
            var itemsSource = EnumTypeToItemsSourceCollection(flaggedEnumType);

            foreach (var item in itemsSource)
                item.IsSelected = (item.IntegerValueMember | value) == value;

            return itemsSource;
        }
    }
}
