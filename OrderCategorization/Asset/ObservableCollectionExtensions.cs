﻿using OrderCategorization.Asset.Attributes;
using OrderCategorization.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace OrderCategorization.Asset
{
    public static class ObservableCollectionExtensions
    {
        public static void SetModelOrderProperty<T>(this ObservableCollection<T> entities) where T : Model
        {
            if (entities != null && entities.Count > 0)
            {
                var modelAttribute = typeof(T).GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;
                var hasTotalRow = modelAttribute?.HasTotalRow;

                for (int i = 0; i < entities.Count; i++)
                    entities[i].Order = i + 1;

                //if (typeof(T) == typeof(SaleLog) || typeof(T) == typeof(BuyLog))
                //{
                //    var totalEntry = entities.FirstOrDefault(x => x.IsTotalRow);

                //    if (typeof(T) == typeof(SaleLog))
                //    {
                //        var totalEntryAsSaleLog = totalEntry as SaleLog;

                //        totalEntryAsSaleLog.Person = new Person { FullName = "مجموع کل" };
                //        totalEntryAsSaleLog.LastSale = new Sale
                //        {
                //            DateTime = ". . ."
                //        };
                        
                //        var transaction = new Transaction();

                //        try
                //        {
                //            transaction.MustTransactedValue = entities.Where(x => !(x as Model).IsTotalRow).Sum(x => (x as SaleLog).LastSale.TotalPrice);

                //        }
                //        catch {}
                //        try
                //        {
                //            transaction.TotalInToStore = entities.Where(x => !(x as Model).IsTotalRow).Sum(x => (x as SaleLog).LastSale.Transaction.TotalInToStore);

                //        }
                //        catch {}
                //        try
                //        {
                //            transaction.TotalOutFromStore = entities.Where(x => !(x as Model).IsTotalRow).Sum(x => (x as SaleLog).LastSale.Transaction.TotalOutFromStore);

                //        }
                //        catch {}

                //        totalEntryAsSaleLog.LastSale.Transaction = transaction;

                //        totalEntry.NotifyPropertyChanged(nameof(totalEntryAsSaleLog.LastSale));
                //    }
                //    else
                //    {
                //        var totalEntryAsBuyLog = totalEntry as BuyLog;

                //        totalEntryAsBuyLog.Person = new Person { FullName = "مجموع کل" };
                //        totalEntryAsBuyLog.LastBuy = new Buy
                //        {
                //            Transaction = new Transaction
                //            {
                //                MustTransactedValue = entities.Where(x => !(x as Model).IsTotalRow).Sum(x => (x as BuyLog).LastBuy.TotalPrice),
                //                TotalInToStore = entities.Where(x => !(x as Model).IsTotalRow).Sum(x => (x as BuyLog).LastBuy.Transaction.TotalInToStore),
                //                TotalOutFromStore = -1 * entities.Where(x => !(x as Model).IsTotalRow).Sum(x => (x as BuyLog).LastBuy.Transaction.TotalOutFromStore),
                //            },
                //            DateTime = ". . ."
                //        };
                //    }
                //}
                //else
                //{
                //    if (hasTotalRow.HasValue && hasTotalRow.Value)
                //    {
                //        var totalEntry = entities.FirstOrDefault(x => x.IsTotalRow);

                //        if (totalEntry != null)
                //        {
                //            foreach (var property in typeof(T).GetProperties())
                //            {
                //                var totalAttribute = property.GetCustomAttribute(typeof(TotalAttribute)) as TotalAttribute;

                //                if (totalAttribute != null)
                //                {
                //                    hasTotalRow = true;
                //                    switch (totalAttribute.TotalType)
                //                    {
                //                        case Enums.TotalAttributeType.Title:
                //                            property.SetValue(totalEntry, totalAttribute.Title);
                //                            break;
                //                        case Enums.TotalAttributeType.Count:
                //                            break;
                //                        case Enums.TotalAttributeType.Sum:
                //                            property.SetValue(totalEntry, entities.Where(x => !(x as Model).IsTotalRow).Sum(entity => (long)property.GetValue(entity)));
                //                            break;
                //                        case Enums.TotalAttributeType.Average:
                //                            break;
                //                        case Enums.TotalAttributeType.Max:
                //                            break;
                //                        case Enums.TotalAttributeType.Min:
                //                            break;
                //                        default:
                //                            break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
            }
        }

        public static Tuple<T, string> SearchInHierarchicalCollection<T>(this ObservableCollection<T> entities, PropertyInfo keyProperty, object selectedId) where T : Model
        {
            var itemType = entities.FirstOrDefault().GetType();
            var nodeQueue = new Queue<Tuple<T, string>>();

            int index = 0;
            foreach (var item in entities)
                nodeQueue.Enqueue(new Tuple<T, string>(item, $"{index++}"));

            var childrenProperty = itemType.GetProperty("Children");

            while (nodeQueue.Count > 0)
            {
                var currentNode = nodeQueue.Dequeue();

                if (keyProperty.GetValue(currentNode.Item1).ToString() == selectedId.ToString())
                    return currentNode;

                dynamic children = childrenProperty.GetValue(currentNode.Item1);

                index = 0;
                if (children != null && children.Count > 0)
                    foreach (var item in children)
                        nodeQueue.Enqueue(new Tuple<T, string>(item, $"{currentNode.Item2},{index++}"));
            }

            return null;
        }

        //public static ObservableCollection<T> Include<T, TForeign>(this ObservableCollection<T> entities, string propertyName, string foreignKeyPropertyName) where T : Model where TForeign : Model
        //{
        //    return CacheUtil.BulkInclude<T, TForeign>(entities, propertyName, foreignKeyPropertyName);
        //}
    }
}
