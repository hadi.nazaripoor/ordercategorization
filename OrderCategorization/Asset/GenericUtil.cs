﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.DataAccess.Model;

namespace OrderCategorization.Asset
{
    public class GenericUtil
    {
        public static ObservableCollection<T> ToObservableCollection<T>(List<T> list) where T : Model
        {
            var observableCollection = new ObservableCollection<T>();

            foreach (var item in list)
                observableCollection.Add(item);

            return observableCollection;
        }

        public static ObservableCollection<Model> ToObservableCollectionOfModel<T>(ObservableCollection<T> list) where T : Model
        {
            var observableCollection = new ObservableCollection<Model>();

            foreach (var item in list)
                observableCollection.Add(item);

            return observableCollection;
        }

        public static void AddDontCareEntry<T>(ObservableCollection<T> inputCollection, T newEntry) where T : Model
        {
            var modelAttribute = typeof(T).GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

            var keyProperty = typeof(T).GetProperty(modelAttribute.KeyPropertyName);
            var titleProperty = typeof(T).GetProperty(modelAttribute.TitlePropertyName);

            inputCollection.Insert(0, newEntry);

            dynamic keyValue = Convert.ChangeType(Constants.DONT_CARE_VALUE, keyProperty.PropertyType);
            
            keyProperty.SetValue(newEntry,  keyValue);
            titleProperty.SetValue(newEntry, "بدون اهمیت");
        }
    }
}
