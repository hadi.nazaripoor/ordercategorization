﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset
{
    public class DeepCloneUtil
    {
        public static T Clone<T>(object obj, bool deep = false)
        {
            if (!(obj is T))
            {
                throw new Exception("Cloning object must match output type");
            }

            return (T)Clone(obj, deep);
        }

        public static object Clone(object obj, bool deep)
        {
            if (obj == null)
            {
                return null;
            }

            Type objType = obj.GetType();

            if (objType.IsPrimitive || objType == typeof(string) || objType.GetConstructors().FirstOrDefault(x => x.GetParameters().Length == 0) == null)
            {
                return obj;
            }

            List<PropertyInfo> properties = objType.GetProperties().Where(p => p.Name != "Children").ToList();
            if (deep)
            {
                properties.AddRange(objType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic));
            }

            object newObj = Activator.CreateInstance(objType);

            foreach (var prop in properties)
            {
                if (prop.GetSetMethod() != null)
                {
                    try
                    {
                        object propValue = prop.GetValue(obj, null);
                        object clone = Clone(propValue, deep);
                        prop.SetValue(newObj, clone, null);
                    }
                    catch { }
                }
            }

            return newObj;
        }
    }
}
