﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderCategorization.Asset
{
    public static class StringExtensions
    {
        public static List<string> GetPossiblePermutations(this string stringValue)
        {
            var stringValueParts = stringValue.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return GetPermutation(stringValueParts);
        }

        public static List<string> GetPermutation(List<string> sourceList)
        {
            var output = new List<string>();

            if (sourceList.Count == 1)
                output.Add(sourceList[0]);
            else
                for (int i = 0; i < sourceList.Count; i++)
                {
                    var removedItem = sourceList[i];
                    sourceList.RemoveAt(i);

                    var permutationWithoutThisItem = GetPermutation(sourceList);
                    foreach (var permutation in permutationWithoutThisItem)
                        output.Add(removedItem + " " + permutation);

                    sourceList.Insert(i, removedItem);
                }

            return output;
        }

        public static bool Contains(this string source, string value, StringComparison compareMode)
        {
            if (string.IsNullOrEmpty(source) && !string.IsNullOrEmpty(value))
                return false;

            return source.IndexOf(value, compareMode) >= 0;
        }

        public static bool IsNullOrWhiteSpace(this string source)
        {
            return string.IsNullOrWhiteSpace(source);
        }
    }
}
