﻿using System.IO;

namespace OrderCategorization.Asset
{
    public class TextFileUtil
    {
        public static string ReadFile(string filePath)
        {
            if (filePath.StartsWith("@"))
                filePath = $"{AppUtil.GetExePath()}{filePath}";

            if (File.Exists(filePath))
                return File.ReadAllText(filePath);

            return "";
        }

        public static void WriteFile(string filePath, string contentToWrite)
        {
            if (filePath.StartsWith("@"))
                filePath = $"{AppUtil.GetExePath()}{filePath.Replace("@", "")}";

            StreamWriter streamWriter = null;

            try
            {
                var directoryPath = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                streamWriter = File.AppendText(filePath);
            }
            catch {}

            if (streamWriter != null)
            {
                streamWriter.WriteLine(contentToWrite);
                streamWriter.Close();
            }
        }
    }
}
