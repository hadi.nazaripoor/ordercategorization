﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using OrderCategorization.DataAccess.EF;
using OrderCategorization.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace OrderCategorization.Asset
{
    public class CacheUtil : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        private bool _isDataCachedCompletely = false;
        public bool IsDataCachedCompletely
        {
            get { return _isDataCachedCompletely; }
            set { _isDataCachedCompletely = value; NotifyPropertyChanged(nameof(IsDataCachedCompletely)); }
        }

        private static string _currentFetchingTable = "";

        public static Dictionary<string, List<Model>> InMemoryDatabase = new Dictionary<string, List<Model>>();
        public static Dictionary<string, ModelDatabaseSchema> ModelsDatabaseSchemaDictionary = new Dictionary<string, ModelDatabaseSchema>();
        public static Dictionary<string, int> ProductsNameToIdDictionary = null;

        public static List<T> GetAll<T>(bool containsDeleted = false) where T : Model
        {
            var nameOfEntity = typeof(T).Name;

            var listToReturn = new List<T>();
            List<Model> rawCollection;

            if (InMemoryDatabase[nameOfEntity] != null)
            {
                rawCollection = InMemoryDatabase[nameOfEntity];
            }
            else if (_currentFetchingTable == nameOfEntity)
            {
                while (_currentFetchingTable == nameOfEntity)
                {
                    Thread.Sleep(500);
                }

                rawCollection = InMemoryDatabase[nameOfEntity];
            }
            else
            {
                rawCollection = GetCollection(typeof(T));
            }

            if (rawCollection != null)
            {
                foreach (var item in rawCollection)
                {
                    listToReturn.Add((T)item);
                }
            }

            return listToReturn;
        }

        //public static void SyncData<T>(Func<T, bool> whereClause, DataSyncType dataSyncType) where T : Model
        //{
        //    var nameOfEntity = typeof(T).Name;
        //    var foreignProperties = ModelsDatabaseSchemaDictionary[nameOfEntity].ForeignProperties;
        //    var manipulatedEntities = new List<T>();

        //    using (var dbContext = new DatabaseContext())
        //    {
        //        var newList = new List<T>();
        //        var dbSet = dbContext.Set<T>().Where(whereClause).AsQueryable();

        //        foreach (var foreignProperty in foreignProperties)
        //        {
        //            dbSet = dbSet.Include(foreignProperty.Item1.Name);
        //        }

        //        manipulatedEntities = dbSet.ToList();
        //    }

        //    if (manipulatedEntities.Count > 0)
        //        SyncData(manipulatedEntities, dataSyncType);
        //}

        public static void SyncData<T>(List<T> manipulatedEntities, DataSyncType dataSyncType) where T : Model
        {
            //await Task.Run(() =>
            //{
                var nameOfEntity = typeof(T).Name;
                while (!InMemoryDatabase.ContainsKey(nameOfEntity))
                {
                    Thread.Sleep(2000);
                }

                var rawCollection = InMemoryDatabase[nameOfEntity];

                if (rawCollection != null)
                {
                    PropertyInfo keyProperty = ModelsDatabaseSchemaDictionary[nameOfEntity].KeyProperty;

                    var manipulatedItemsDictionary = new Dictionary<dynamic, T>();

                    foreach (var item in manipulatedEntities)
                        manipulatedItemsDictionary.Add(keyProperty.GetValue(item), item);

                    for (int i = 0; i < rawCollection.Count; i++)
                    {
                        dynamic thisItemId = keyProperty.GetValue(rawCollection[i]);

                        if (manipulatedItemsDictionary.ContainsKey(thisItemId))
                        {
                            if (dataSyncType == DataSyncType.CreateUpdate)
                            {
                                rawCollection[i] = manipulatedItemsDictionary[thisItemId];
                                PropagateChange(manipulatedItemsDictionary[thisItemId]);
                            }
                            else
                            {
                                rawCollection.RemoveAt(i);
                                i--;
                            }

                            manipulatedItemsDictionary.Remove(thisItemId);
                        }
                    }

                    foreach (var keyValue in manipulatedItemsDictionary)
                    {
                        rawCollection.Add(keyValue.Value);
                    }
                }
            //});
        }

        private static async Task PropagateChange<T>(T modelToPropageteChange)
        {
            await Task.Run(() =>
            {
                var nameOfEntity = typeof(T).Name;
                if (ModelsDatabaseSchemaDictionary.ContainsKey(nameOfEntity))
                {
                    var allRelatedProperties = new List<PropertyInfo>();

                    foreach (var modelsDatabaseSchemaesKeyValues in ModelsDatabaseSchemaDictionary)
                    {
                        var modelsDatabaseSchemaes = modelsDatabaseSchemaesKeyValues.Value;

                        var relatedProperty = modelsDatabaseSchemaes.ForeignProperties.FirstOrDefault(p => p.Item1.PropertyType == typeof(T));
                        if (relatedProperty != null)
                        {
                            if (InMemoryDatabase.ContainsKey(modelsDatabaseSchemaesKeyValues.Key))
                            {
                                var modelToPropagateChangeKeyId = ModelsDatabaseSchemaDictionary[nameOfEntity].KeyProperty.GetValue(modelToPropageteChange);

                                foreach (var entityToUpdate in InMemoryDatabase[modelsDatabaseSchemaesKeyValues.Key])
                                {
                                    var entityToUpdateForeignId = relatedProperty.Item2.GetValue(entityToUpdate);

                                    if (entityToUpdateForeignId != null && modelToPropagateChangeKeyId != null && entityToUpdateForeignId.ToString() == modelToPropagateChangeKeyId.ToString())
                                        relatedProperty.Item1.SetValue(entityToUpdate, modelToPropageteChange);
                                }
                            }
                        }
                    }
                }
            });
        }

        public static T GetFirstOrDefault<T>(Func<T, bool> whereClause) where T : Model
        {
            var allEntitiesToSearch = GetAll<T>();

            return allEntitiesToSearch.FirstOrDefault(whereClause);
        }

        public static List<T> Where<T>(Func<T, bool> whereClause) where T : Model
        {
            var allEntities = GetAll<T>();
            var allEntitiesToReturn = new List<T>();

            foreach (var entity in allEntities.Where(whereClause))
                allEntitiesToReturn.Add(entity);

            return allEntitiesToReturn;
        }

        private async Task FetchData()
        {
            await Task.Run(() =>
            {
                var namespaceString = "OrderCategorization.DataAccess.Model";
                var allTypes = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsPublic && t.Namespace == namespaceString).ToList();

                using (var dbContext = new DatabaseContext())
                {
                    foreach (var type in allTypes)
                    {
                        try
                        {
                            if (InMemoryDatabase[type.Name] == null && _currentFetchingTable != type.Name)
                            {
                                _currentFetchingTable = type.Name;

                                var newList = GetCollection(type);
                                InMemoryDatabase[type.Name] = newList;
                                
                                _currentFetchingTable = "";
                            }
                        }
                        catch (Exception exx) { Console.WriteLine(exx.ToString()); }
                    }
                }
            });
        }

        private static List<Model> GetCollection(Type type)
        {
            List<Tuple<PropertyInfo, PropertyInfo>> foreignProperties = null;

            if (ModelsDatabaseSchemaDictionary.ContainsKey(type.Name))
                foreignProperties = ModelsDatabaseSchemaDictionary[type.Name].ForeignProperties;
            else
            {
                foreignProperties = new List<Tuple<PropertyInfo, PropertyInfo>>();
                PropertyInfo keyProperty = null;

                foreach (var property in type.GetProperties())
                {
                    var foreignAttribute = (ForeignKeyAttribute)property.GetCustomAttribute(typeof(ForeignKeyAttribute));
                    if (foreignAttribute != null)
                    {
                        var propertyKey = type.GetProperty(foreignAttribute.Name);
                        foreignProperties.Add(new Tuple<PropertyInfo, PropertyInfo>(property, propertyKey));
                    }
                    else
                    {
                        var keyAttribute = (KeyAttribute)property.GetCustomAttribute(typeof(KeyAttribute));
                        if (keyAttribute != null)
                        {
                            keyProperty = property;
                        }
                    }
                }

                var modelDatabaseSchema = new ModelDatabaseSchema() { KeyProperty = keyProperty, ForeignProperties = foreignProperties };

                ModelsDatabaseSchemaDictionary.Add(type.Name, modelDatabaseSchema);
            }

            using (var dbContext = new DatabaseContext())
            {
                var newList = new List<Model>();
                var dbSet = dbContext.Set(type).AsQueryable();

                foreach (var foreignProperty in foreignProperties)
                {
                    if (foreignProperty.Item1.PropertyType != type)
                        dbSet = dbSet.Include(foreignProperty.Item1.Name);
                }

                foreach (var item in dbSet)
                    newList.Add((Model)item);

                return newList;
            }
        }

        public CacheUtil()
        {
            var namespaceString = "OrderCategorization.DataAccess.Model";
            var allTypes = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsPublic && t.Namespace == namespaceString).ToList();

            foreach (var type in allTypes)
            {
                InMemoryDatabase.Add(type.Name, null);
            }

            FetchData();
        }
    }
}
