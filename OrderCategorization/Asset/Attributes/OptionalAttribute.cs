﻿using System;
using OrderCategorization.Asset.Enums;

namespace OrderCategorization.Asset.Attributes
{
    public class OptionalAttribute : Attribute
    {
        public SearchControlType SearchControlEnum { get; set; }
        public object DefaultValue { get; set; }

        public OptionalAttribute()
        {
        }
    }
}
