﻿using OrderCategorization.Asset.Enums;
using System;

namespace OrderCategorization.Asset.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TotalAttribute : Attribute
    {
        public TotalAttributeType TotalType { get; set; }
        public string Title { get; set; }
    }
}
