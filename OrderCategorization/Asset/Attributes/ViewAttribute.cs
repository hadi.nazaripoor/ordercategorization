﻿using System;

namespace OrderCategorization.Asset.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    class ViewAttribute : Attribute
    {
        public string ViewTitle { get; set; }
        public string KeyPropertyName { get; set; }
    }
}
