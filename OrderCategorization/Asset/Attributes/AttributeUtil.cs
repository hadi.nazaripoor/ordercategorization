﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.Attributes
{
    public class AttributeUtil
    {
        public static string[] GetModelAttributeValues(ModelAttribute attribute)
        {
            return new string[] { attribute.KeyPropertyName, attribute.TitlePropertyName };
        }
    }
}
