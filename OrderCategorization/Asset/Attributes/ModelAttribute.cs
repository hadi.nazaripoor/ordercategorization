﻿using System;
using OrderCategorization.DataAccess.Model;

namespace OrderCategorization.Asset.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ModelAttribute : Attribute
    {
        public string SingleEntityTitle { get; set; }
        public string MultipleEntitiesTitle { get; set; }
        public string KeyPropertyName { get; set; }
        public string ParentPropertyName { get; set; }
        public string OrderPropertyName { get; set; }
        public bool IsOrderDesc { get; set; }
        public string TitlePropertyName { get; set; }
        public string ComboBoxSearchPropertiesNames { get; set; }
        public bool HasTransaction { get; set; }
        public bool IsFirstEntityPermanent { get; set; }
        public bool HasTotalRow { get; set; }
    }
}
