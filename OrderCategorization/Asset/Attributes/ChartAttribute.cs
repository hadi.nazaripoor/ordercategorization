﻿using System;

namespace OrderCategorization.Asset.Attributes
{
    public class ChartAttribute : Attribute
    {
        public bool IsTitleOfChart { get; set; }
        public string StringFormat { get; set; }
        public bool IncludedInPie { get; set; }
        public string Color { get; set; }
    }
}
