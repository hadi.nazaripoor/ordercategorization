﻿using OrderCategorization.Asset.Enums;
using System;
using System.Linq.Expressions;

namespace OrderCategorization.Asset.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SearchAttribute : Attribute
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public SearchControlType SearchControlEnum { get; set; }
        public object DefaultValue { get; set; }
        public string EnumOrRepositoryName { get; set; }
        public bool HasDontCare { get; set; }
        public bool HasStartAndEndControlForDates { get; set; }
        public WhereClauseMethodNames OptionalWhereClause { get; set; } = WhereClauseMethodNames.None;
    }
}
