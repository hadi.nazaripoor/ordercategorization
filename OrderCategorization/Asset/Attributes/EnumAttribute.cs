﻿using System;

namespace OrderCategorization.Asset.Attributes
{
    [AttributeUsage(AttributeTargets.Enum)]
    public class EnumAttribute : Attribute
    {
        public string NonSelectedStringFormat { get; set; }
        public string SingleSelectedStringFormat { get; set; }
        public string MultipleSelectedStringFormat { get; set; }
    }
}
