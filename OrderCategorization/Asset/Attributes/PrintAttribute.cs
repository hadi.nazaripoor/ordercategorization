﻿using System;

namespace OrderCategorization.Asset.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PrintAttribute : Attribute
    {
        public string Title { get; set; }
        public string StringFormat { get; set; } = "";
    }
}
