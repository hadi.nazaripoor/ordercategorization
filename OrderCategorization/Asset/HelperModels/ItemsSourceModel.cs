﻿using OrderCategorization.DataAccess.Model;
using System;

namespace OrderCategorization.Asset.HelperModels
{
    public class ItemsSourceModel : Model
    {
        private string _displayMember;
        private string _stringValueMember;
        private int _integerValueMember;
        private string _otherInfo;

        public string DisplayMember
        {
            get{return _displayMember;}
            set{_displayMember = value;NotifyPropertyChanged(nameof(DisplayMember)); }
        }

        public string StringValueMember
        {
            get { return _stringValueMember; }
            set { _stringValueMember = value; NotifyPropertyChanged(nameof(StringValueMember)); }
        }

        public int IntegerValueMember
        {
            get { return _integerValueMember; }
            set { _integerValueMember = value; NotifyPropertyChanged(nameof(IntegerValueMember)); }
        }

        public string OtherInfo
        {
            get{return _otherInfo;}
            set{_otherInfo = value;NotifyPropertyChanged(nameof(OtherInfo)); }
        }

        public ItemsSourceModel()
        {
        }
    }
}
