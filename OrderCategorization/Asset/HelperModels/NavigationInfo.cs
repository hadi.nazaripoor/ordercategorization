﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.Model;
using System;

namespace OrderCategorization.Asset.HelperModels
{
    public class NavigationInfo : Model
    {
        private string _title;
        private Uri _navigatePageSource;
        private string _shortcut;
        private string _iconSource;

        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        public string Shortcut
        {
            get { return _shortcut; }
            set { _shortcut = value; NotifyPropertyChanged(nameof(Shortcut)); }
        }

        public Uri NavigationPageSource
        {
            get { return _navigatePageSource; }
            set { _navigatePageSource = value; NotifyPropertyChanged(nameof(NavigationPageSource)); }
        }

        public string IconSource
        {
            get { return _iconSource; }
            set { _iconSource = value; NotifyPropertyChanged(nameof(IconSource)); }
        }

        public NavigationInfo()
        {
            IconSource = "/OrderCategorization;component/Images/QuickAccessIcons/NoIcon.png";
        }
    }
}
