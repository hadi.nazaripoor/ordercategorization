﻿using System;
using OrderCategorization.DataAccess.Model;
using OrderCategorization.Asset.Enums;
using System.Collections.Generic;

namespace OrderCategorization.Asset.HelperModels
{
    public class DialogePageModel : Model
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DialogePageTypes DialogePageTypeEnum { get; set; }
        public List<DialogeButtonContentModel> ButtonsContent { get; set; } = new List<DialogeButtonContentModel>();
    }
}
