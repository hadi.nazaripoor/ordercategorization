﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.HelperModels
{
    public class ReverseNavigationProeprtyModel
    {
        public Type EntityType { get; set; }
        public string KeyPropertyName { get; set; }
        public List<InfluencedProperty> InfluencedProperties { get; set; } = new List<InfluencedProperty>();
    }

    public class InfluencedProperty
    {
        public Type EntityType { get; set; }
        public string PropertyName { get; set; }
    }
}
