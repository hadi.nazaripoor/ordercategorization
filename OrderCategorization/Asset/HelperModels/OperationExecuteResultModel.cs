﻿using OrderCategorization.Asset.Enums;

namespace OrderCategorization.Asset.HelperModels
{
    public class OperationExecuteResultModel
    {
        public OperationExecuteResultType OperationExecuteResultTypeEnum { get; set; }
        public string ResultTitle { get; set; }
        public string ResultDescription { get; set; }
        public string UsefulParameter { get; set; }
        public OperationExecuteResultModel()
        {
        }
    }
}
