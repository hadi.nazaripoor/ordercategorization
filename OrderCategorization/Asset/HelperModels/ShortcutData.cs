﻿using OrderCategorization.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OrderCategorization.Asset.HelperModels
{
    public class ShortcutData : Model
    {
        private ModifierKeys _modifierKeys;
        private Key _key;
        private string _shortcutOutput;
        private bool _isValidShortcut;

        public ModifierKeys ModifierKeys
        {
            get { return _modifierKeys; }
            set { _modifierKeys = value; NotifyPropertyChanged(nameof(ModifierKeys)); }
        }

        public Key Key
        {
            get { return _key; }
            set { _key = value; NotifyPropertyChanged(nameof(Key)); RefreshResult(); }
        }

        public string ShortcutOutput
        {
            get { return _shortcutOutput; }
            set { _shortcutOutput = value; NotifyPropertyChanged(nameof(ShortcutOutput)); }
        }

        public bool IsValidShortcut
        {
            get { return _isValidShortcut; }
            set { _isValidShortcut = value; NotifyPropertyChanged(nameof(IsValidShortcut)); }
        }

        private void RefreshResult()
        {
            var keyAsString = "";
            IsValidShortcut = true;

            if (Key >= Key.F1 && Key <= Key.F19)
                keyAsString = Key.ToString();

            else if (Key >= Key.D0 && Key <= Key.D9)
                keyAsString = ((char)(48 + Key - Key.D0)).ToString();

            else if (Key >= Key.NumPad0 && Key <= Key.NumPad9)
                keyAsString = ((char)(48 + Key - Key.NumPad0)).ToString();

            else if (Key >= Key.A && Key <= Key.Z)
                keyAsString = ((char)(65 + Key - Key.A)).ToString();

            else if (Key == Key.Decimal || Key == Key.OemPeriod)
                keyAsString = ".";

            else if (Key == Key.Space)
                keyAsString = Key.ToString();

            ModifierKeys &= (ModifierKeys.Alt | ModifierKeys.Control);

            if (keyAsString == "" || (ModifierKeys == 0 && (Key <= Key.F1 || Key >= Key.F19)))
            {
                ShortcutOutput = "";
                IsValidShortcut = false;
            }
            else
            {
                ShortcutOutput = ModifierKeys.ToString().Replace(", ", " + ");
                if (ShortcutOutput == "None") ShortcutOutput = "";
                ShortcutOutput += ShortcutOutput == "" ? keyAsString : " + " + keyAsString;
            }
        }

        public ShortcutData()
        {
        }
    }
}
