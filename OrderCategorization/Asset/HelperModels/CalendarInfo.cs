﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.Model;
using System;

namespace OrderCategorization.Asset.HelperModels
{
    public class CalendarInfo : Model
    {
        private int _year;
        private int _month;
        private int _day;
        private string _content;
        private bool _isYearMode;

        private int _lastDayOfPreMonth;
        private int _firstDayOfThisMonth;
        private int _thisMonthDaysCount;

        public int Year
        {
            get { return _year; }
            set { _year = value; NotifyPropertyChanged(nameof(Year)); SetContent(); SetDayNumbers(); }
        }

        public int Month
        {
            get { return _month; }
            set { _month = value; NotifyPropertyChanged(nameof(Month)); SetContent(); SetDayNumbers(); }
        }

        public int Day
        {
            get { return _day; }
            set { _day = value; NotifyPropertyChanged(nameof(Day)); SetContent(); }
        }

        public string Content
        {
            get { return _content; }
            set { _content = value; NotifyPropertyChanged(nameof(Content)); }
        }

        public bool IsYearMode
        {
            get { return _isYearMode; }
            set { _isYearMode = value; NotifyPropertyChanged(nameof(IsYearMode)); SetContent(); }
        }

        public int LastDayOfPreMonth
        {
            get { return _lastDayOfPreMonth; }
            set { _lastDayOfPreMonth = value; NotifyPropertyChanged(nameof(LastDayOfPreMonth)); }
        }

        public int FirstDayOfThisMonth
        {
            get { return _firstDayOfThisMonth; }
            set { _firstDayOfThisMonth = value; NotifyPropertyChanged(nameof(FirstDayOfThisMonth)); }
        }

        public int ThisMonthDaysCount
        {
            get { return _thisMonthDaysCount; }
            set { _thisMonthDaysCount = value; NotifyPropertyChanged(nameof(ThisMonthDaysCount)); }
        }

        private void SetContent()
        {
            Content = IsYearMode ? Year.ToString("0000") : $"{((PersianMonth)Month).ToString()} {Year.ToString("0000")}";
        }

        private void SetDayNumbers()
        {
            if (Year == 0)
                return;

            int lastDayOfPreMonth = 0, firstDayOfthisMonth = 0, thisMonthDaysCount = 0;
            PersianUtil.SetMonthInfo(Year, Month, ref lastDayOfPreMonth, ref firstDayOfthisMonth, ref thisMonthDaysCount);

            LastDayOfPreMonth = lastDayOfPreMonth;
            FirstDayOfThisMonth = firstDayOfthisMonth;
            ThisMonthDaysCount = thisMonthDaysCount;
        }

        public string ComposeDate()
        {
            return $@"{Year.ToString("0000")}/{Month.ToString("00")}/{Day.ToString("00")}";
        }

        public void ParseDate(string fullDate)
        {
            string[] datePart = fullDate.Split('/');

            Year = int.Parse(datePart[0]);
            Month = int.Parse(datePart[1]);
            Day = int.Parse(datePart[2]);
        }
    }
}
