﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.Model;
using System;

namespace OrderCategorization.Asset.HelperModels
{
    public class DialogeButtonContentModel : Model
    {
        private string _content;
        private DialogeResultType _dialogeResultEnum;

        public string Content
        {
            get { return _content; }
            set { _content = value; NotifyPropertyChanged(nameof(Content)); }
        }

        public DialogeResultType DialogeResultEnum
        {
            get { return _dialogeResultEnum; }
            set { _dialogeResultEnum = value; NotifyPropertyChanged(nameof(DialogeResultEnum)); }
        }
    }
}
