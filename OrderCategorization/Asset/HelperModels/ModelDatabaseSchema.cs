﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrderCategorization.Asset.HelperModels
{
    public class ModelDatabaseSchema
    {
        public PropertyInfo KeyProperty { get; set; }
        public List<Tuple<PropertyInfo, PropertyInfo>> ForeignProperties { get; set; } // ForeignProperty, ForeignKey

        public ModelDatabaseSchema()
        {
        }
    }
}
