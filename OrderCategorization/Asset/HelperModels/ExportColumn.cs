﻿using OrderCategorization.DataAccess.Model;
using System;

namespace OrderCategorization.Asset.HelperModels
{
    public class ExportColumn : Model
    {
        private string _propertyName;
        private string _title;
        private double _width;
        private string _stringFormat;

        public string PropertyName
        {
            get { return _propertyName; }
            set { _propertyName = value; NotifyPropertyChanged(nameof(PropertyName)); }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        public double Width
        {
            get { return _width; }
            set { _width = value; NotifyPropertyChanged(nameof(Width)); }
        }

        public string StringFormat
        {
            get { return _stringFormat; }
            set { _stringFormat = value; NotifyPropertyChanged(nameof(StringFormat)); }
        }

        public ExportColumn()
        {
        }
    }
}