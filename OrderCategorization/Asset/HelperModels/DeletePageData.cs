﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset.HelperModels
{
    public class DeletePageData
    {
        public string DeletePageTitle { get; set; }
        public string SingleItemDeletionDescription { get; set; }
        public string MultipleItemDeletionDescription { get; set; }
        public bool RealDelete { get; set; }
        public DeletePageData()
        {
        }
    }
}
