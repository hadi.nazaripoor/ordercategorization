﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using OrderCategorization.DataAccess.EF;
using OrderCategorization.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using LinqExpr = System.Linq.Expressions;

namespace OrderCategorization.Asset
{
    public class DataIntegrityUtil
    {
        // زمانی که یک تراکنش را درج کرده و یا حذف می کنیم، باید این متد صدا زده شود؛
        // اولین کار این است که آیدی تراکنش به موجودیت ایجاد کننده این تراکنش افزوده شود
        // آخرین کاری که باید انجام شود این است که به بالانس شخص (در صورتی که شخصی وجود داشت)، افزوده شده یا کاسته شود
        public static void AddOrRemoveTransactions(ObservableCollection<Transaction> transactions, bool isAdd)
        {
            foreach (var transaction in transactions)
            {
                AddTransactionIdToCauserEntity(transaction, isAdd);

                if (transaction.PersonId != null)
                {
                    var personToChangeBalance = PersonRepository.GetSpecial(transaction.PersonId.Value);
                    if (personToChangeBalance != null)
                    {
                        var valueToIncreaseOrDecrease = transaction.MustTransactedValue + transaction.TotalOutFromStore - transaction.TotalInToStore;

                        if (isAdd)
                            personToChangeBalance.Balance += valueToIncreaseOrDecrease;
                        else
                            personToChangeBalance.Balance -= valueToIncreaseOrDecrease;

                        PersonRepository.AddOrUpdate(personToChangeBalance);
                    }
                }
            }
        }

        // متد زیر برای افزودن آیدی تراکنش به موجودیت ایجاد کننده آن و برعکس، افزودن آیدی موجودیت به تراکنش استفاده می شود.
        private static void AddTransactionIdToCauserEntity(Transaction transaction, bool isAdd)
        {
            var transactionCauserModel = transaction.GetModel();
            var transactionCauserTypeEnum = (TransactionCauserType)transaction.TransactionCauserTypeEnum;
            var transactionId = transaction.TransactionId;

            var modelName = (transactionCauserTypeEnum).ToString();
            var repositoryName = $"{modelName}Repository";
            var modelType = Type.GetType($"OrderCategorization.DataAccess.Model.{modelName}");
            var repositoryType = Type.GetType($"OrderCategorization.DataAccess.EF.{repositoryName}");

            if (modelType != null && repositoryType != null && transactionCauserModel != null)
            {
                var modelTypeTransactionProperty = modelType.GetProperty("TransactionId");
                if (modelTypeTransactionProperty != null)
                {
                    if (isAdd)
                        modelType.GetProperty("TransactionId").SetValue(transactionCauserModel, transactionId);
                    else
                        modelType.GetProperty("TransactionId").SetValue(transactionCauserModel, null);
                }

                var addOrUpdtaeMethod = repositoryType.GetMethod("AddOrUpdate");
                var param = new object[] { transactionCauserModel };
                addOrUpdtaeMethod.Invoke(repositoryType, param);
            }
        }

        public static void EntityDeletePropagation<T>(T entityToDelete) where T : class
        {
            var reverseNavigationProeprtyModel = App.Current.FindResource(typeof(T).Name) as ReverseNavigationProeprtyModel;

            if (reverseNavigationProeprtyModel != null)
            {
                var propertyInfo = reverseNavigationProeprtyModel.EntityType.GetProperty(reverseNavigationProeprtyModel.KeyPropertyName);
                using (var dbContext = new DatabaseContext())
                {
                    foreach (var navigationProperty in reverseNavigationProeprtyModel.InfluencedProperties)
                    {
                        if (propertyInfo != null)
                        {
                            var propertyValue = propertyInfo.GetValue(entityToDelete);

                            var sqlTableName = dbContext.GetTableName(navigationProperty.EntityType);
                            var dbSet = dbContext.Set(navigationProperty.EntityType);
                            var sqlCommand = $"Update {sqlTableName} Set {navigationProperty.PropertyName} = 1 Where {navigationProperty.PropertyName} = {propertyValue}";

                            DatabaseUtil.ExecuteNonQuery(sqlCommand);
                        }
                    }
                }
            }
        }
    }
}
