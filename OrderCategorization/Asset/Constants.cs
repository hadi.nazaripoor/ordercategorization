﻿namespace OrderCategorization.Asset
{
    public class Constants
    {
        public const string ALL_SELECTED = "/OrderCategorization;component/Images/CommonIcon/Selection.png",
                            ALL_UNSELECTED = "/OrderCategorization;component/Images/CommonIcon/NonSelection.png",
                            SUBSECTION_SELECTED = "/OrderCategorization;component/Images/CommonIcon/SubSection.png";

        public const string CASH_IMAGE = "/OrderCategorization;component/Images/CommonIcon/SafeBox.png",
                            BANK_IMAGE = "/OrderCategorization;component/Images/CommonIcon/Bank.png";

        public const string SUCCESS_FOREGROUND = "#FF43D36A",
                            ERROR_FOREGROUND = "#FFF75353",
                            WARNING_FOREGROUND = "#FFFFAE00",
                            INFO_FOREGROUND = "#FF247AFF",
                            HINT_NORMAL_FOREGROUND = "#FFD1D1D1";

        public const int LENGTH_FOR_DATES = 10,
                         LENGTH_FOR_TIMES = 5,
                         LENGTH_FOR_DATE_TIMES = 17,
                         LENGTH_FOR_CALL_NUMBERS = 11,
                         SMALL_STRING_LENGTH = 16,
                         MEDIUM_STRING_LENGTH = 64,
                         LARGE_STRING_LENGTH = 256,
                         VERY_LARGE_STRING_LENGTH = 2048;

        public const string DEFAULT_VALUE_FOR_TODAY = "Today";

        public const short DONT_CARE_VALUE = -113;

        public const string KEY_AES = "AES();123;P@$$#0&",
                            USER_KEY = "380804915C5CFC2984058E27970B61E3",
                            SAFE_KEY1_SETTINGS_KEY = "FK",
                            SAFE_KEY2_SETTINGS_KEY = "SK";
        // ABBASI BOOSHEHR
        //SAFE_KEY1 = "6C29E4E95DAAF39B6C5CE41D5A0B4F273E89B64A2F0BC5FB3EBCB67D7383D78E18F890B9097A9F6B182C90EC41D99992",
        //SAFE_KEY2 = "48E73815BCBB8FACFF9E6CFA47A4178921A397FEE94DD29C963BD26E2DE3290ADC8059659BEC1E63E1F8800E95525596";
        // DANIAL ...
        //USER_KEY = "A94439165C5CFC29F262E940E16C0684",
        //SAFE_KEY1 = "174C8F0D08CD9EBE177F8F40B06FD26580B2F8737134082580E5F8A6FDC219B74616BED63797C3783C39B4F930D53DDA",
        //SAFE_KEY2 = "1609167A6419D3BFDB8B3C8FE5CF871B9F98D9C7B772F90D28629CB5D594349774823281CE8B7346BA5FEB5399791311";
    }
}
