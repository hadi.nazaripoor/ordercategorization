﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Controls;
using OrderCategorization.DataAccess.Model;
using OrderCategorization.Views.CUs;
using OrderCategorization.Views.Lists;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace OrderCategorization.Asset
{
    public class KeyboardUtil
    {
        public static int? DetectNumber(Key key)
        {
            if (key >= Key.D0 && key <= Key.D9 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.None)
                return (int)key - 34;

            if (key >= Key.NumPad0 && key <= Key.NumPad9)
                return (int)key - 74;

            return null;
        }

        public static CalculatorOperators DetectOperator(Key key)
        {
            if (key == Key.Add || (key == Key.OemPlus && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift))
                return CalculatorOperators.Add;

            if (key == Key.Subtract || (key == Key.OemMinus && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.None))
                return CalculatorOperators.Subtract;

            if (key == Key.Multiply || (key == Key.Oem8 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift))
                return CalculatorOperators.Multiply;

            if (key == Key.Divide || (key == Key.OemBackslash && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.None))
                return CalculatorOperators.Divide;

            if (key == Key.Enter || key == Key.Return || (key == Key.OemPlus && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.None))
                return CalculatorOperators.Equal;

            return CalculatorOperators.None;
        }

        public static bool IsDot(Key key)
        {
            return key == Key.Decimal || key == Key.OemPeriod;
        }

        public void CreateUpdateBasePage_PreviewKeyDown<T>(KeyEventArgs e, CreateUpdateBasePage<T> createUpdatePage) where T : Model
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && (e.Key == Key.Enter || e.Key == Key.Escape))
            {
                if (typeof(T) != typeof(Shortcut))
                    switch (e.Key)
                    {
                        case Key.Enter:
                            var focusedControl = (DependencyObject)Keyboard.FocusedElement;
                            if (focusedControl != null && focusedControl is TextBox && (focusedControl as TextBox).AcceptsReturn)
                                return;

                            while (focusedControl != null)
                            {
                                if (focusedControl is ComboBoxUC)
                                    return;

                                focusedControl = VisualTreeHelper.GetParent(focusedControl);
                            }

                            createUpdatePage.AddOrUpdate();
                            break;

                        case Key.Escape:
                            createUpdatePage.Close(null, null);
                            break;

                        default:
                            return; // RETURN => e.Handled = false; => Propagate KeyDown Event
                    }

                e.Handled = true;
            }
        }

        public void ListBasePage_PreviewKeyDown<T>(KeyEventArgs e, ListBasePage<T> listBasePage) where T: Model
        {
            if (Keyboard.Modifiers == ModifierKeys.Control || e.Key == Key.Delete || e.Key == Key.Escape)
            {
                switch (e.Key)
                {
                    case Key.N:
                        listBasePage.AddNew(null, null);
                        break;

                    case Key.Delete:
                        listBasePage.Delete(null, new RoutedEventArgs());
                        break;

                    case Key.Escape:
                        listBasePage.Close(null, null);
                        break;

                    case Key.P:
                        listBasePage.Print();
                        break;

                    case Key.A:
                        listBasePage.AllSelectDeselectButton_Click(null, null);
                        break;

                    case Key.F:
                        listBasePage.StartSearch(null, null);
                        break;

                    default:
                        return; // RETURN => e.Handled = false; => Propagate KeyDown Event
                }

                e.Handled = true;
            }
        }
    }
}
