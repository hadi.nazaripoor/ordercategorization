﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCategorization.Asset
{
    public class DatabaseUtil
    {
        public static string GetConnectionString()
        {
            var encryptedConnectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            return encryptedConnectionString;
            //return CryptographyUtil.Decrypt(encryptedConnectionString);
        }

        private static string GetDatabaseName()
        {
            var connectionString = GetConnectionString();
            var startOfDatabaseName = connectionString.IndexOf("OrderCategorization");
            var LenghtOfDatabaseName = connectionString.IndexOf(";Integrated") - startOfDatabaseName;
            return connectionString.Substring(connectionString.IndexOf("OrderCategorization"), LenghtOfDatabaseName);
        }

        public static OperationExecuteResultModel Backup(string backupPath = "", bool isSilent = false)
        {
            var operationResult = new OperationExecuteResultModel();

            try
            {
                if (string.IsNullOrWhiteSpace(backupPath) || !Directory.Exists(backupPath))
                {
                    var folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
                    System.Windows.Forms.DialogResult result = folderBrowserDialog.ShowDialog();

                    if (result == System.Windows.Forms.DialogResult.OK && Directory.Exists(folderBrowserDialog.SelectedPath))
                        backupPath = folderBrowserDialog.SelectedPath;
                    else
                    {
                        operationResult.OperationExecuteResultTypeEnum = OperationExecuteResultType.Canceled;
                        operationResult.ResultTitle = "لغو عملیات";
                        operationResult.ResultDescription = "عملیات پشتیبان گیری توسط کاربر لغو شد";

                        return operationResult;
                    }
                }

                if (!backupPath.EndsWith(@"\")) backupPath += @"\";

                var backupFullName = $@"{backupPath}[{PersianUtil.PersianDate().Replace('/', '-')}][{PersianUtil.PersianTime().Replace(':', '-')}].bak";
                var databaseName = GetDatabaseName();
                var masterConnectionString = GetConnectionString().Replace(databaseName, "master");

                var backupCommandText = @"Backup Database @databaseName To Disk = @backupFullName With Init";

                SqlParameter[] pr =
                {
                    new SqlParameter("@backupFullName", backupFullName),
                    new SqlParameter("@databaseName", databaseName),
                };

                ExecuteNonQuery(masterConnectionString, backupCommandText, pr);

                operationResult.OperationExecuteResultTypeEnum = OperationExecuteResultType.Done;
                operationResult.ResultTitle = "انجام عملیات";
                operationResult.ResultDescription = "عملیات پشتیبان گیری با موفقیت انجام شد";
                operationResult.UsefulParameter = backupFullName;

                return operationResult;
            }
            catch (Exception exception)
            {
                operationResult.OperationExecuteResultTypeEnum = OperationExecuteResultType.Failed;
                operationResult.ResultTitle = "شکست در انجام عملیات";
                operationResult.ResultDescription = "عملیات پشتیبان گیری به شکست انجامید";
                operationResult.UsefulParameter = exception.Message;

                return operationResult;
            }
        }

        public static OperationExecuteResultModel Restore(string backupPath)
        {
            var operationResult = new OperationExecuteResultModel();
            var backupFileName = "";

            try
            {
                var openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Backup File (*.bak)|*.bak|All files (*.*)|*.*";

                if (!string.IsNullOrWhiteSpace(backupPath) || Directory.Exists(backupPath))
                    openFileDialog.InitialDirectory = backupPath;

                var dialogResult = openFileDialog.ShowDialog();
                if (dialogResult.HasValue && dialogResult.Value)
                {
                    backupFileName = openFileDialog.FileName;

                    var databaseName = GetDatabaseName();
                    var masterConnectionString = GetConnectionString().Replace(databaseName, "master");

                    KillRelatedProcesses(databaseName);

                    var restoreCommandText = "Restore Database @databaseName From disk = @backupPath With Replace";

                    SqlParameter[] pr =
                    {
                        new SqlParameter("@backupPath", backupFileName),
                        new SqlParameter("@databaseName", databaseName),
                    };


                    ExecuteNonQuery(masterConnectionString, restoreCommandText, pr);
                    SqlConnection.ClearAllPools();

                    operationResult.OperationExecuteResultTypeEnum = OperationExecuteResultType.Done;
                    operationResult.ResultTitle = "انجام عملیات";
                    operationResult.ResultDescription = "عملیات بازیابی با موفقیت انجام شد";

                    return operationResult;
                }
                else
                {
                    operationResult.OperationExecuteResultTypeEnum = OperationExecuteResultType.Canceled;
                    operationResult.ResultTitle = "لغو عملیات";
                    operationResult.ResultDescription = "عملیات بازیابی توسط کاربر لغو شد";

                    return operationResult;
                }
            }
            catch (Exception exception)
            {
                operationResult.OperationExecuteResultTypeEnum = OperationExecuteResultType.Failed;
                operationResult.ResultTitle = "شکست در انجام عملیات";
                operationResult.ResultDescription = "عملیات بازیابی به شکست انجامید";
                operationResult.UsefulParameter = exception.Message;

                return operationResult;
            }
        }

        private static void KillRelatedProcesses(string databaseName)
        {
            var masterConnectionString = GetConnectionString().Replace(databaseName, "master");

            var processListCommandText = "Exec sp_who;";
            var killCommand = "";

            var processListDataReader = ExecuteReader(masterConnectionString, processListCommandText);

            var databaseProcesseIds = new List<short>();

            while (processListDataReader.Read())
                if (processListDataReader["dbname"].ToString() == databaseName)
                    databaseProcesseIds.Add((short)processListDataReader["spid"]);

            processListDataReader.Close();

            if (databaseProcesseIds.Count() > 0)
                foreach (var spid in databaseProcesseIds)
                    killCommand += "KILL " + spid.ToString();

            ExecuteNonQuery(masterConnectionString, killCommand, null);
        }

        #region SQL EXECUTER REGION
        public static void ExecuteNonQuery(string commandText, params SqlParameter[] commandParameters)
        {
            ExecuteNonQuery(GetConnectionString(), commandText, commandParameters);
        }

        public static void ExecuteNonQuery(string connectionString, string commandText, params SqlParameter[] commandParameters)
        {
            SqlCommand sqlCommand = new SqlCommand();
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            PrepareCommand(sqlConnection, sqlCommand, null, commandText, commandParameters);

            sqlCommand.ExecuteNonQuery();
            sqlCommand.Parameters.Clear();
        }

        public static SqlDataReader ExecuteReader(string commandText, params SqlParameter[] commandParameters)
        {
            return ExecuteReader(GetConnectionString(), commandText, commandParameters);
        }

        public static SqlDataReader ExecuteReader(string connectionString, string commandText, params SqlParameter[] commandParameters)
        {
            SqlCommand sqlCommand = new SqlCommand();
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            try
            {
                PrepareCommand(sqlConnection, sqlCommand, null, commandText, commandParameters);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                sqlCommand.Parameters.Clear();
                return dataReader;
            }
            catch
            {
                sqlConnection.Close();
                return null;
            }
        }

        private static void PrepareCommand(SqlConnection sqlConnection, SqlCommand sqlCommand, SqlTransaction trans, string commandText, SqlParameter[] commandParmeters)
        {
            if (sqlConnection.State != ConnectionState.Open)
                sqlConnection.Open();

            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = commandText;

            if (trans != null)
                sqlCommand.Transaction = trans;

            if (commandParmeters != null)
            {
                foreach (SqlParameter parm in commandParmeters)
                    sqlCommand.Parameters.Add(parm);
            }
        }
        #endregion
    }
}
