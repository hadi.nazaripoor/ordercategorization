﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.EF;
using OrderCategorization.DataAccess.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using LinqExpr = System.Linq.Expressions;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// Interaction logic for SearchUC.xaml
    /// </summary>
    public partial class SearchUC : UserControl
    {
        List<int> NotPassed = new List<int>();
        List<PropertyInfo> SearchProperties;

        public SearchUC()
        {
            InitializeComponent();
        }

        public void InitializeSearchForModel<T>(ObservableCollection<T> allList, ObservableCollection<T> toDisplayList, T searchPrimaryModel = null) where T : Model
        {
            SearchButtonsForReportGrid.Visibility = Visibility.Collapsed;
            ClearSearchParametersButton.Visibility = Visibility.Visible;

            allList.CollectionChanged += AllList_CollectionChanged;

            NotPassed.Clear();

            for (int i = 0; i < allList.Count; i++)
                NotPassed.Add(0);

            var ModelAttribute = typeof(T).GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

            TitleTextBlock.Text = $"جستجو در لیست {ModelAttribute.MultipleEntitiesTitle}";

            SearchMemberStackPanel.Children.Clear();

            SearchProperties = typeof(T).GetProperties().Where(p => p.GetCustomAttribute(typeof(SearchAttribute), false) != null).ToList();

            var totalPassNumber = (int)Math.Pow(2, SearchProperties.Count) - 1;
            var propertyPassNumber = 1;

            foreach (var property in SearchProperties)
            {
                var searchAttribute = property.GetCustomAttribute(typeof(SearchAttribute)) as SearchAttribute;

                if (searchAttribute != null)
                {
                    switch (searchAttribute.SearchControlEnum)
                    {
                        case SearchControlType.TextBox:
                            {
                                var newTextBox = UiUtil.GetTextBox(searchAttribute.Title);
                                newTextBox.Name = $"_{propertyPassNumber}";

                                newTextBox.TextChanged += (s, e) =>
                                {
                                    toDisplayList.Clear();
                                    var thisPropertyPassNumber = int.Parse((s as TextBox).Name.Substring(1));
                                    var propertyPassNumberReverse = totalPassNumber - thisPropertyPassNumber;

                                    for (int i = 0; i < allList.Count(); i++)
                                    {
                                        if (property.GetValue(allList[i]).ToString().Contains(newTextBox.Text, StringComparison.InvariantCultureIgnoreCase))
                                            NotPassed[i] &= propertyPassNumberReverse;
                                        else
                                            NotPassed[i] |= thisPropertyPassNumber;

                                        if (NotPassed[i] == 0 || allList[i].IsTotalRow)
                                            toDisplayList.Add(allList[i]);
                                    }
                                    toDisplayList.SetModelOrderProperty();
                                };
                                SearchMemberStackPanel.Children.Add(newTextBox);

                                if (searchPrimaryModel != null)
                                    newTextBox.Text = property.GetValue(searchPrimaryModel).ToString();
                            }
                            break;

                        case SearchControlType.ComboBox:
                            {
                                var newComboBox = UiUtil.GetComboBox(searchAttribute.Title);
                                newComboBox.Name = $"_{propertyPassNumber}";

                                if (!searchAttribute.EnumOrRepositoryName.Contains("Repository"))
                                {
                                    newComboBox.DisplayMemberPath = "DisplayMember";
                                    newComboBox.SelectedValuePath = "IntegerValueMember";

                                    var enumType = Type.GetType($"OrderCategorization.Asset.Enums.{searchAttribute.EnumOrRepositoryName}");
                                    newComboBox.ItemsSource = EnumUtil.EnumTypeToItemsSourceCollection(enumType, true);
                                }
                                else
                                {
                                    var modelName = searchAttribute.EnumOrRepositoryName.Split(new string[] { "Repository" }, StringSplitOptions.RemoveEmptyEntries)[0];
                                    var modelType = Type.GetType($"OrderCategorization.DataAccess.Model.{modelName}");
                                    ModelAttribute modelAttribute = modelType.GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

                                    newComboBox.SelectedValuePath = modelAttribute.KeyPropertyName;
                                    newComboBox.DisplayMemberPath = modelAttribute.TitlePropertyName;

                                    var repositoryType = Type.GetType($"OrderCategorization.DataAccess.EF.{searchAttribute.EnumOrRepositoryName}");
                                    var getAllMethod = searchAttribute.HasDontCare ? repositoryType.GetMethod("GetAllWithDontCare") : repositoryType.GetMethod("GetAll");

                                    if (getAllMethod.GetParameters().Count() > 0)
                                        newComboBox.ItemsSource = (IList)getAllMethod.Invoke(repositoryType, new object[] { false }); // containsDeleted
                                    else
                                        newComboBox.ItemsSource = (IList)getAllMethod.Invoke(repositoryType, null);
                                }

                                newComboBox.SelectionChanged += (s, e) =>
                                {
                                    toDisplayList.Clear();
                                    var thisPropertyPassNumber = int.Parse((s as ComboBox).Name.Substring(1));
                                    var propertyPassNumberReverse = totalPassNumber - thisPropertyPassNumber;

                                    for (int i = 0; i < allList.Count(); i++)
                                    {
                                        if (Convert.ToInt64(newComboBox.SelectedValue) == Constants.DONT_CARE_VALUE || Convert.ToInt64(property.GetValue(allList[i])) == Convert.ToInt64(newComboBox.SelectedValue))
                                            NotPassed[i] &= propertyPassNumberReverse;
                                        else
                                            NotPassed[i] |= thisPropertyPassNumber;

                                        if (NotPassed[i] == 0 || allList[i].IsTotalRow)
                                            toDisplayList.Add(allList[i]);
                                    }
                                    toDisplayList.SetModelOrderProperty();
                                };
                                SearchMemberStackPanel.Children.Add(newComboBox);

                                if (searchPrimaryModel != null)
                                {
                                    newComboBox.SelectedValue = property.GetValue(searchPrimaryModel);
                                    newComboBox.RaiseEvent(
                                        new SelectionChangedEventArgs(Selector.SelectionChangedEvent,
                                        new List<ComboBoxItem> { newComboBox.Items[0] as ComboBoxItem },
                                        new List<ComboBoxItem> { newComboBox.SelectedItem as ComboBoxItem }));
                                }
                            }
                            break;

                        case SearchControlType.ComboBoxUC:
                            {
                                var newComboBoxUC = UiUtil.GetComboBoxUC(searchAttribute.Title);
                                newComboBoxUC.Name = $"_{propertyPassNumber}";

                                var modelName = searchAttribute.EnumOrRepositoryName.Split(new string[] { "Repository" }, StringSplitOptions.RemoveEmptyEntries)[0];
                                var modelType = Type.GetType($"OrderCategorization.DataAccess.Model.{modelName}");

                                if (modelType == typeof(Product))
                                    newComboBoxUC.ItemsSource = GenericUtil.ToObservableCollectionOfModel(ProductRepository.GetAllWithDontCare());

                                newComboBoxUC.SelectionChanged += (s, e) =>
                                {
                                    toDisplayList.Clear();
                                    var thisPropertyPassNumber = int.Parse((s as ComboBoxUC).Name.Substring(1));
                                    var propertyPassNumberReverse = totalPassNumber - thisPropertyPassNumber;

                                    for (int i = 0; i < allList.Count(); i++)
                                    {
                                        if (Convert.ToInt64(newComboBoxUC.SelectedValue) < 0 || Convert.ToInt64(property.GetValue(allList[i])) == Convert.ToInt64(newComboBoxUC.SelectedValue))
                                            NotPassed[i] &= propertyPassNumberReverse;
                                        else
                                            NotPassed[i] |= thisPropertyPassNumber;

                                        if (NotPassed[i] == 0 || allList[i].IsTotalRow)
                                            toDisplayList.Add(allList[i]);
                                    }
                                    toDisplayList.SetModelOrderProperty();
                                };
                                SearchMemberStackPanel.Children.Add(newComboBoxUC);

                                if (searchPrimaryModel != null)
                                {
                                    newComboBoxUC.SelectedValue = property.GetValue(searchPrimaryModel);
                                }
                            }
                            break;

                        case SearchControlType.DatePickerUC:
                            {
                                if (!searchAttribute.HasStartAndEndControlForDates)
                                {
                                    var newDatePickerUC = GetDatePickerUC(searchAttribute);
                                    newDatePickerUC.Name = $"_{propertyPassNumber}";

                                    newDatePickerUC.DateChanged += (s, e) =>
                                    {
                                        toDisplayList.Clear();
                                        var thisPropertyPassNumber = int.Parse((s as DatePickerUC).Name.Substring(1));
                                        var propertyPassNumberReverse = totalPassNumber - thisPropertyPassNumber;

                                        for (int i = 0; i < allList.Count(); i++)
                                        {
                                            var propertyValue = property.GetValue(allList[i]).ToString();
                                            if (string.IsNullOrEmpty(newDatePickerUC.Date) || propertyValue.Contains(newDatePickerUC.Date))
                                                NotPassed[i] &= propertyPassNumberReverse;
                                            else
                                                NotPassed[i] |= thisPropertyPassNumber;

                                            if (NotPassed[i] == 0 || allList[i].IsTotalRow)
                                                toDisplayList.Add(allList[i]);
                                        }
                                        toDisplayList.SetModelOrderProperty();
                                    };
                                    SearchMemberStackPanel.Children.Add(newDatePickerUC);

                                    if (searchPrimaryModel != null)
                                        newDatePickerUC.Date = property.GetValue(searchPrimaryModel).ToString();
                                }
                                else
                                {
                                    var newDatePickerUCForStart = GetDatePickerUC(searchAttribute, " (شروع)");
                                    newDatePickerUCForStart.Name = $"S_{propertyPassNumber}";

                                    var newDatePickerUCForEnd = GetDatePickerUC(searchAttribute, " (پایان)");
                                    newDatePickerUCForEnd.Name = $"E_{propertyPassNumber}";

                                    newDatePickerUCForStart.DateChanged += (s, e) =>
                                    {
                                        toDisplayList.Clear();
                                        var thisPropertyPassNumber = int.Parse((s as DatePickerUC).Name.Substring(2));
                                        var propertyPassNumberReverse = totalPassNumber - thisPropertyPassNumber;

                                        var startDate = newDatePickerUCForStart.Date;
                                        var endDate = newDatePickerUCForEnd.Date;

                                        if (string.IsNullOrWhiteSpace(startDate))
                                            startDate = "0000/00/00";

                                        if (string.IsNullOrWhiteSpace(endDate))
                                            endDate = "9999/99/99";

                                        for (int i = 0; i < allList.Count(); i++)
                                        {
                                            var propertyValue = property.GetValue(allList[i]).ToString();

                                            if ((propertyValue.CompareTo(startDate) >= 0 && propertyValue.CompareTo(endDate) <= 0))
                                                NotPassed[i] &= propertyPassNumberReverse;
                                            else
                                                NotPassed[i] |= thisPropertyPassNumber;

                                            if (NotPassed[i] == 0 || allList[i].IsTotalRow)
                                                toDisplayList.Add(allList[i]);
                                        }
                                        toDisplayList.SetModelOrderProperty();
                                    };
                                    newDatePickerUCForEnd.DateChanged += (s, e) =>
                                    {
                                        toDisplayList.Clear();
                                        var thisPropertyPassNumber = int.Parse((s as DatePickerUC).Name.Substring(2));
                                        var propertyPassNumberReverse = totalPassNumber - thisPropertyPassNumber;

                                        var startDate = newDatePickerUCForStart.Date;
                                        var endDate = newDatePickerUCForEnd.Date;

                                        if (string.IsNullOrWhiteSpace(startDate))
                                            startDate = "0000/00/00";

                                        if (string.IsNullOrWhiteSpace(endDate))
                                            endDate = "9999/99/99";

                                        for (int i = 0; i < allList.Count(); i++)
                                        {
                                            var propertyValue = property.GetValue(allList[i]).ToString();

                                            if ((propertyValue.CompareTo(startDate) >= 0 && propertyValue.CompareTo(endDate) <= 0))
                                                NotPassed[i] &= propertyPassNumberReverse;
                                            else
                                                NotPassed[i] |= thisPropertyPassNumber;

                                            if (NotPassed[i] == 0 || allList[i].IsTotalRow)
                                                toDisplayList.Add(allList[i]);
                                        }
                                        toDisplayList.SetModelOrderProperty();
                                    };

                                    SearchMemberStackPanel.Children.Add(newDatePickerUCForStart);
                                    SearchMemberStackPanel.Children.Add(newDatePickerUCForEnd);

                                    if (searchPrimaryModel != null)
                                    {
                                        newDatePickerUCForStart.Date = property.GetValue(searchPrimaryModel).ToString();
                                        newDatePickerUCForEnd.Date = property.GetValue(searchPrimaryModel).ToString();
                                    }
                                }
                            }
                            break;
                        case SearchControlType.CheckBox:
                            break;
                        default:
                            break;
                    }

                    propertyPassNumber *= 2;
                }
            }

            if (SearchMemberStackPanel.Children.Count > 0)
                SearchMemberStackPanel.Children[0].Focus();
        }

        private void AllList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                for (int i = 0; i < e.NewItems.Count; i++)
                    NotPassed.Add(0);

            else if (e.Action == NotifyCollectionChangedAction.Remove)
                foreach (var item in e.OldItems)
                    if (item is Model)
                    {
                        var itemAsModelOrder = (item as Model).Order;
                        if (NotPassed.Count >= itemAsModelOrder)
                            NotPassed.RemoveAt((int)itemAsModelOrder - 1);
                        else
                            NotPassed.RemoveAt(NotPassed.Count - 1);
                    }
        }

        private LinqExpr.Expression<Func<T, bool>> GetLambdaExpression<T>(PropertyInfo property, object propertyValue, WhereClauseMethodNames whereClauseMethodNames)
        {
            var parameter = LinqExpr.Expression.Parameter(typeof(T), "x");
            var linqProperty = LinqExpr.Expression.Property(parameter, property.Name);
            var constantValueForSearch = LinqExpr.Expression.Constant(Convert.ChangeType(propertyValue, property.PropertyType));

            MethodInfo methodName = null;

            switch (whereClauseMethodNames)
            {
                case WhereClauseMethodNames.Equals:
                    methodName = property.PropertyType.GetMethod("Equals", new Type[] { property.PropertyType });
                    break;
                case WhereClauseMethodNames.Contains:
                    methodName = typeof(string).GetMethod("Contains");
                    break;
                case WhereClauseMethodNames.GreaterThanOrEquals:
                case WhereClauseMethodNames.LessThanOrEquals:
                    methodName = typeof(string).GetMethod("CompareTo", new Type[] { typeof(string) });
                    break;
                default:
                    break;
            }


            var equalsMethod = LinqExpr.Expression.Call(linqProperty, methodName, constantValueForSearch);

            if (whereClauseMethodNames == WhereClauseMethodNames.GreaterThanOrEquals || whereClauseMethodNames == WhereClauseMethodNames.LessThanOrEquals)
            {
                LinqExpr.BinaryExpression binaryExpression = null;

                if (whereClauseMethodNames == WhereClauseMethodNames.LessThanOrEquals)
                    binaryExpression = LinqExpr.Expression.LessThanOrEqual(equalsMethod, LinqExpr.Expression.Constant(0));
                else
                    binaryExpression = LinqExpr.Expression.GreaterThanOrEqual(equalsMethod, LinqExpr.Expression.Constant(0));

                return LinqExpr.Expression.Lambda<Func<T, bool>>(binaryExpression, parameter);
            }
            else
                return LinqExpr.Expression.Lambda<Func<T, bool>>(equalsMethod, parameter);
        }

        public static LinqExpr.Expression<Func<T, bool>> PropertyLessThanOrEqualString<T, String>(PropertyInfo property, String value)
        {
            var parent = LinqExpr.Expression.Parameter(typeof(T));
            var expressionBody = CompareLessThanOrEqualTo(LinqExpr.Expression.Property(parent, property), LinqExpr.Expression.Constant(value));
            return LinqExpr.Expression.Lambda<Func<T, bool>>(expressionBody, parent);
        }

        public static LinqExpr.Expression CompareLessThanOrEqualTo(LinqExpr.Expression e1, LinqExpr.Expression e2)
        {
            var compare = LinqExpr.Expression.Call(typeof(string), "Compare", null, new[] { e1, e2 });

            return LinqExpr.Expression.LessThanOrEqual(compare, LinqExpr.Expression.Constant(0));
        }

        public static LinqExpr.Expression<Func<T, bool>> PropertyGreaterThanOrEqualString<T, String>(PropertyInfo property, String value)
        {
            var parent = LinqExpr.Expression.Parameter(typeof(T));
            var expressionBody = CompareGreaterThanOrEqualTo(LinqExpr.Expression.Property(parent, property), LinqExpr.Expression.Constant(value));
            return LinqExpr.Expression.Lambda<Func<T, bool>>(expressionBody, parent);
        }

        public static LinqExpr.Expression CompareGreaterThanOrEqualTo(LinqExpr.Expression e1, LinqExpr.Expression e2)
        {
            var compare = LinqExpr.Expression.Call(typeof(string), "Compare", null, new[] { e1, e2 });

            return LinqExpr.Expression.LessThanOrEqual(compare, LinqExpr.Expression.Constant(0));
        }

        private DatePickerUC GetDatePickerUC(SearchAttribute propertyAttribute, string startOrEnd = "")
        {
            return new DatePickerUC { Tag = $"{propertyAttribute.Title}{startOrEnd}", MinHeight = 42 };
        }

        private void HideMenuButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = (MainWindow)App.Current.MainWindow;
            mainWindow.HideSearchBar();
        }

        private void ClearSearchParametersButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var searchItem in SearchMemberStackPanel.Children)
            {
                if (searchItem is TextBox) { (searchItem as TextBox).Text = ""; }
                else if (searchItem is ComboBox) { (searchItem as ComboBox).SelectedIndex = 0; }
                else if (searchItem is ComboBoxUC) { (searchItem as ComboBoxUC).SelectedValue = -1; }
                else if (searchItem is DatePickerUC) { (searchItem as DatePickerUC).Date = ""; }
            }
        }
    }
}
