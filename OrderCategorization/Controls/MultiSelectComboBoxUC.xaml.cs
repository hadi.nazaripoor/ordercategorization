﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.EF;
using OrderCategorization.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// Interaction logic for MultiSelectComboBoxUC.xaml
    /// </summary>
    public partial class MultiSelectComboBoxUC : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        public Type CreatePageType
        {
            get { return (Type)base.GetValue(CreatePageTypeProperty); }
            set { base.SetValue(CreatePageTypeProperty, value); NotifyPropertyChanged(nameof(CreatePageType)); }
        }
        public static readonly DependencyProperty CreatePageTypeProperty = DependencyProperty.Register("CreatePageType", typeof(Type), typeof(MultiSelectComboBoxUC), new PropertyMetadata(null));

        public ObservableCollection<Model> ItemsSource
        {
            get { return (ObservableCollection<Model>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<Model>), typeof(MultiSelectComboBoxUC), new PropertyMetadata(null));

        public ObservableCollection<Model> SelectedItems
        {
            get { return (ObservableCollection<Model>)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }
        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register("SelectedItems", typeof(ObservableCollection<Model>), typeof(MultiSelectComboBoxUC), new PropertyMetadata(new ObservableCollection<Model>()));

        public MultiSelectComboBoxUC()
        {
            InitializeComponent();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var senderAsButton = sender as Button;
            var selectedItem = senderAsButton.Tag as Tag;

            if (selectedItem != null)
                SelectedItems.Remove(selectedItem);
        }

        private void SuggestionComboBox_SelectionFinalized(object sender, Model e)
        {
            if (!SelectedItems.Contains(e))
                SelectedItems.Add(e);

            SuggestionComboBox.Input = "";
        }

        public void SaveTags(TransactionCauserType transactionCauserType, long transactionCauserId)
        {
            var documentTag = DocumentTagRepository.GetBuyTransactionCauserAndId((int)transactionCauserType, transactionCauserId);
            if (documentTag == null)
                documentTag = new DocumentTag { TransactionCauserId = transactionCauserId, TransactionCauserTypeEnum = (int)transactionCauserType };

            if (SelectedItems != null && SelectedItems.Count > 0)
                documentTag.TagsIds = "," + string.Join(",", SelectedItems.Select(x => (x as Tag).TagId)) + ",";
            else
                documentTag.TagsIds = "";

            DocumentTagRepository.AddOrUpdate(documentTag);
        }

        public void LoadTags(TransactionCauserType transactionCauserType, long transactionCauserId)
        {
            if (ItemsSource != null && ItemsSource.Count > 0)
            {
                var documentTag = DocumentTagRepository.GetBuyTransactionCauserAndId((int)transactionCauserType, transactionCauserId);

                SelectedItems = new ObservableCollection<Model>();
                if (documentTag != null)
                    foreach (var tagIdAsString in documentTag.TagsIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        var itemInItemsSource = ItemsSource.FirstOrDefault(x => (x as Tag).TagId.ToString() == tagIdAsString);

                        if (itemInItemsSource != null)
                        {
                            SelectedItems.Add(itemInItemsSource);
                            itemInItemsSource.IsSelected = true;
                        }
                    }
            }
        }
    }
}
