﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.HelperModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// این کنترل در حال حاضر برای انتخاب چند آیتم از موارد یک اینام استفاده می شود و در آینده شاید به استفاده از آن برای آیتم های لیست هم برسم
    /// </summary>
    public partial class EnumMultiSelectComboBoxUC : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public short Value
        {
            get { return (short)base.GetValue(ValueProperty); }
            set { base.SetValue(ValueProperty, value); NotifyPropertyChanged(nameof(Value)); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(short), typeof(EnumMultiSelectComboBoxUC), new FrameworkPropertyMetadata((short)0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DataChanged));

        public Type SourceEnumType
        {
            get { return (Type)base.GetValue(SourceEnumTypeProperty); }
            set { base.SetValue(SourceEnumTypeProperty, value); NotifyPropertyChanged(nameof(SourceEnumType)); }
        }
        public static readonly DependencyProperty SourceEnumTypeProperty = DependencyProperty.Register("SourceEnumTypeProperty", typeof(Type), typeof(EnumMultiSelectComboBoxUC), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DataChanged));

        private static void DataChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var userControl = (EnumMultiSelectComboBoxUC)dependencyObject;

            var sourceEnumType = userControl.SourceEnumType;
            var value = userControl.Value;
            var enumValues = userControl.EnumValues;

            if (sourceEnumType != null)
            {
                if (enumValues.Count == 0)
                {
                    var enumItems = EnumUtil.EnumTypeToItemsSourceCollection(sourceEnumType);
                    foreach (var enumItem in enumItems)
                        enumValues.Add(enumItem);
                }

                foreach (var enumValue in enumValues)
                    enumValue.IsSelected = value != 0 && (value | (short)enumValue.IntegerValueMember) == value;
            }
        }

        public ObservableCollection<ItemsSourceModel> EnumValues { get; set; } = new ObservableCollection<ItemsSourceModel>();
        public EnumMultiSelectComboBoxUC()
        {
            InitializeComponent();
        }

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.StaysOpen = false;
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.StaysOpen = true;
        }

        private void SelectDeselectButton_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Button).DataContext as ItemsSourceModel;
            Value ^= (short)item.IntegerValueMember;

            //item.IsSelected = !item.IsSelected;
        }

        private void ToggleVisibility(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.IsOpen = !SuggestionGrid.IsOpen;
        }
    }
}
