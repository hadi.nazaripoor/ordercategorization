﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using System;
using System.Windows;
using System.Windows.Controls;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// Interaction logic for SubMenuUC.xaml
    /// </summary>
    public partial class SubMenuItemUC : UserControl
    {
        public PageOrOperationType PageOrOperationEnum
        {
            get { return (PageOrOperationType)base.GetValue(PageOrOperationEnumProperty); }
            set { base.SetValue(PageOrOperationEnumProperty, value); }
        }
        public static readonly DependencyProperty PageOrOperationEnumProperty = DependencyProperty.Register("PageOrOperationEnum", typeof(PageOrOperationType), typeof(SubMenuItemUC), new FrameworkPropertyMetadata(PageOrOperationType.None, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, PageOrOperationChanged));

        public NavigationInfo NavigationInfo { get; set; } = new NavigationInfo();

        public static void PageOrOperationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SubMenuItemUC thisSubMenuItem = d as SubMenuItemUC;

            var navigationInfo = NavigationInfoComposer.Compose(thisSubMenuItem.PageOrOperationEnum);

            thisSubMenuItem.NavigationInfo.Title = navigationInfo.Title;
            thisSubMenuItem.NavigationInfo.Shortcut = navigationInfo.Shortcut;
            thisSubMenuItem.NavigationInfo.NavigationPageSource = navigationInfo.NavigationPageSource;
            thisSubMenuItem.NavigationInfo.IconSource = navigationInfo.IconSource;
        }


        public SubMenuItemUC()
        {
            InitializeComponent();
        }

        public event EventHandler UC_Clicked;

        private void NavigationButton_Click(object sender, RoutedEventArgs e)
        {
            UC_Clicked?.Invoke(this, EventArgs.Empty);
        }
    }
}
