﻿using OrderCategorization.Controls.EventArguments;
using System;
using System.Windows;
using System.Windows.Controls;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// Interaction logic for ManipulationMenuUC.xaml
    /// </summary>
    public partial class ManipulationMenuUC : UserControl
    {
        public long EntityId
        {
            get { return (long)base.GetValue(EntityIdProperty); }
            set { base.SetValue(EntityIdProperty, value); }
        }
        public static readonly DependencyProperty EntityIdProperty = DependencyProperty.Register("EntityId", typeof(long), typeof(ManipulationMenuUC));

        public Visibility UpdateButtonVisibility
        {
            get { return (Visibility)base.GetValue(UpdateButtonVisibilityProperty); }
            set { base.SetValue(UpdateButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty UpdateButtonVisibilityProperty = DependencyProperty.Register("UpdateButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));

        public Visibility DeleteButtonVisibility
        {
            get { return (Visibility)base.GetValue(DeleteButtonVisibilityProperty); }
            set { base.SetValue(DeleteButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty DeleteButtonVisibilityProperty = DependencyProperty.Register("DeleteButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));

        public Visibility PrintButtonVisibility
        {
            get { return (Visibility)base.GetValue(PrintButtonVisibilityProperty); }
            set { base.SetValue(PrintButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty PrintButtonVisibilityProperty = DependencyProperty.Register("PrintButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));
        
        public Visibility SendMessageButtonVisibility
        {
            get { return (Visibility)base.GetValue(SendMessageButtonVisibilityProperty); }
            set { base.SetValue(SendMessageButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty SendMessageButtonVisibilityProperty = DependencyProperty.Register("SendMessageButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));
        
        public Visibility ChartButtonVisibility
        {
            get { return (Visibility)base.GetValue(ChartButtonVisibilityProperty); }
            set { base.SetValue(ChartButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty ChartButtonVisibilityProperty = DependencyProperty.Register("ChartButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));

        public Visibility MoreButtonVisibility
        {
            get { return (Visibility)base.GetValue(MoreButtonVisibilityProperty); }
            set { base.SetValue(MoreButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty MoreButtonVisibilityProperty = DependencyProperty.Register("MoreButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));

        public Visibility InfoButtonVisibility
        {
            get { return (Visibility)base.GetValue(InfoButtonVisibilityProperty); }
            set { base.SetValue(InfoButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty InfoButtonVisibilityProperty = DependencyProperty.Register("InfoButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));

        public Visibility CardexButtonVisibility
        {
            get { return (Visibility)base.GetValue(CardexButtonVisibilityProperty); }
            set { base.SetValue(CardexButtonVisibilityProperty, value); }
        }
        public static readonly DependencyProperty CardexButtonVisibilityProperty = DependencyProperty.Register("CardexButtonVisibility", typeof(Visibility), typeof(ManipulationMenuUC), new PropertyMetadata(Visibility.Collapsed));

        public event EventHandler<EntityIdEventArg> UpdateButton_Clicked;
        public event EventHandler<EntityIdEventArg> DeleteButton_Clicked;
        public event EventHandler<EntityIdEventArg> PrintButton_Clicked;
        public event EventHandler<EntityIdEventArg> SendMessageButton_Clicked;
        public event EventHandler<EntityIdEventArg> ChartButton_Clicked;
        public event EventHandler<EntityIdEventArg> MoreButton_Clicked;
        public event EventHandler<EntityIdEventArg> InfoButton_Clicked;
        public event EventHandler<EntityIdEventArg> CardexButton_Clicked;

        public ManipulationMenuUC()
        {
            InitializeComponent();
        }

        private void SendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            SendMessageButton_Clicked?.Invoke(sender, new EntityIdEventArg { EntityId = this.EntityId });
        }

        private void ChartButton_Click(object sender, RoutedEventArgs e)
        {
            ChartButton_Clicked?.Invoke(sender, new EntityIdEventArg { EntityId = this.EntityId });
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateButton_Clicked?.Invoke(sender, new EntityIdEventArg { EntityId = this.EntityId });
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            PrintButton_Clicked?.Invoke(sender, new EventArguments.EntityIdEventArg { EntityId = this.EntityId });
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteButton_Clicked?.Invoke(sender, new EventArguments.EntityIdEventArg { EntityId = this.EntityId });
        }

        private void MoreButton_Click(object sender, RoutedEventArgs e)
        {
            MoreButton_Clicked?.Invoke(sender, new EventArguments.EntityIdEventArg { EntityId = this.EntityId });
        }

        private void InfoButton_Click(object sender, RoutedEventArgs e)
        {
            InfoButton_Clicked?.Invoke(sender, new EventArguments.EntityIdEventArg { EntityId = this.EntityId });
        }

        private void CardexButton_Click(object sender, RoutedEventArgs e)
        {
            CardexButton_Clicked?.Invoke(sender, new EventArguments.EntityIdEventArg { EntityId = this.EntityId });
        }
    }
}
