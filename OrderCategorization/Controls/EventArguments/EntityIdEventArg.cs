﻿using System;

namespace OrderCategorization.Controls.EventArguments
{
    public class EntityIdEventArg : EventArgs
    {
        public long EntityId { get; set; }
    }
}
