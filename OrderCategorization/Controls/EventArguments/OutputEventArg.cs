﻿using OrderCategorization.Asset.Enums;
using System;

namespace OrderCategorization.Controls.EventArguments
{
    public class OutputEventArg : EventArgs
    {
        public ImportExportTypes ImportExportTypeEnum { get; set; }
    }
}
