﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.HelperModels;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// Interaction logic for MyDatePicker.xaml
    /// </summary>
    public partial class DatePickerUC : UserControl
    {
        public enum DateSections { Date, DatePart }
        public EventHandler DateChanged;

        public string Date
        {
            get { return (string)GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }
        public static readonly DependencyProperty DateProperty = DependencyProperty.Register("Date", typeof(string), typeof(DatePickerUC), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnDateChanged));

        private static void OnDateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var userControl = (DatePickerUC)d;
            userControl.SectionPropertyChanged(DateSections.Date);
            userControl.DateChanged?.Invoke(userControl, null);
        }

        public CalendarInfo DateInfo { get; set; }
        public bool IsReaction { get; set; }

        int dayToDecolor = -1;

        public void SectionPropertyChanged(DateSections dateSection)
        {
            if (IsReaction)
                return;

            IsReaction = true;

            switch (dateSection)
            {
                case DateSections.Date:
                    if (PersianUtil.IsValidDate(Date))
                    {
                        DateInfo.ParseDate(Date);
                        MonthChanged();
                    }
                    break;
                case DateSections.DatePart:
                    SetCurrentValue(DateProperty, DateInfo.ComposeDate());
                    break;
                default:
                    break;
            }

            IsReaction = false;
        }

        public DatePickerUC()
        {
            InitializeComponent();

            DateInfo = PersianUtil.PersianDateAsCalendarInfo();
            DateSelectionPopup.DataContext = DateInfo;

            dayToDecolor = DateInfo.Day + DateInfo.FirstDayOfThisMonth - 1;
            MonthChanged();
        }

        private void SwitchButton_Click(object sender, RoutedEventArgs e)
        {
            DateInfo.IsYearMode = !DateInfo.IsYearMode;
        }

        private void DayButton_Click(object sender, RoutedEventArgs e)
        {
            var dayButton = (Button)sender;
            DateInfo.Day = int.Parse(dayButton.Content.ToString());

            SectionPropertyChanged(DateSections.DatePart);

            SelectedDayIndicatorRectangle.Visibility = Visibility.Visible;
            SelectedDayIndicatorRectangle.SetValue(Grid.RowProperty, dayButton.GetValue(Grid.RowProperty));
            SelectedDayIndicatorRectangle.SetValue(Grid.ColumnProperty, dayButton.GetValue(Grid.ColumnProperty));

            ToggleVisibility(null, null);
        }

        private void MonthButton_DblClick(object sender, MouseButtonEventArgs e)
        {
            var monthButton = (Button)sender;
            DateInfo.Month = int.Parse(monthButton.Name.Split('_')[1]);

            SwitchButton_Click(null, null);
            MonthChanged();
        }

        private void MonthChanged()
        {
            int i = 1;
            var currentDatePrefix = $"{DateInfo.Year.ToString("0000")}/{DateInfo.Month.ToString("00")}";
            SelectedDayIndicatorRectangle.Visibility = Visibility.Collapsed;

            if (dayToDecolor != -1)
            {
                if (currentDatePrefix == PersianUtil.PersianDate().Substring(0, 7))
                    ((Button)DaysGrid.FindName($"D_{dayToDecolor.ToString()}")).Foreground = Brushes.Green;
                else
                    ((Button)DaysGrid.FindName($"D_{dayToDecolor.ToString()}")).Foreground = Brushes.Black;
            }

            for (; i < DateInfo.FirstDayOfThisMonth; i++)
            {
                var btn = (Button)DaysGrid.FindName($"D_{i.ToString()}");
                btn.IsEnabled = false;

                btn.Content = $"{(DateInfo.LastDayOfPreMonth + 1 - DateInfo.FirstDayOfThisMonth + i).ToString()}";
            }

            for (; i < DateInfo.FirstDayOfThisMonth + DateInfo.ThisMonthDaysCount; i++)
            {
                var btn = (Button)DaysGrid.FindName($"D_{i.ToString()}");

                btn.IsEnabled = true;

                btn.Content = $"{(i - DateInfo.FirstDayOfThisMonth + 1).ToString()}";

                if (btn.Content.ToString() == DateInfo.Day.ToString())
                {
                    SelectedDayIndicatorRectangle.Visibility = Visibility.Visible;
                    SelectedDayIndicatorRectangle.SetValue(Grid.RowProperty, btn.GetValue(Grid.RowProperty));
                    SelectedDayIndicatorRectangle.SetValue(Grid.ColumnProperty, btn.GetValue(Grid.ColumnProperty));
                }
            }

            for (; i <= 42; i++)
            {
                var btn = (Button)DaysGrid.FindName($"D_{i.ToString()}");

                btn.IsEnabled = false;

                btn.Content = $"{(i + 1 - (DateInfo.FirstDayOfThisMonth + DateInfo.ThisMonthDaysCount)).ToString()}";
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            var tempMonth = DateInfo.Month;
            var tempYear = DateInfo.Year;

            tempMonth += DateInfo.IsYearMode ? 12 : 1;

            if (tempMonth > 12)
            {
                tempMonth -= 12;
                tempYear++;
            }

            DateInfo.Year = tempYear;
            DateInfo.Month = tempMonth;

            MonthChanged();
        }

        private void PreButton_Click(object sender, RoutedEventArgs e)
        {
            var tempMonth = DateInfo.Month;
            var tempYear = DateInfo.Year;

            tempMonth -= DateInfo.IsYearMode ? 12 : 1;

            if (tempMonth < 1)
                if (tempYear > 1)
                {
                    tempMonth += 12;
                    tempYear--;
                }
                else
                    tempMonth -= DateInfo.IsYearMode ? 12 : 1;

            DateInfo.Year = tempYear;
            DateInfo.Month = tempMonth;

            MonthChanged();
        }

        private void DateTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SetCurrentValue(DateProperty, PersianUtil.PersianDate());
            SectionPropertyChanged(DateSections.Date);
        }

        private void DateTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void ToggleVisibility(object sender, RoutedEventArgs e)
        {
            DateSelectionPopup.IsOpen = !DateSelectionPopup.IsOpen;
        }

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            DateSelectionPopup.StaysOpen = false;
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            DateSelectionPopup.StaysOpen = true;
        }
    }
}
