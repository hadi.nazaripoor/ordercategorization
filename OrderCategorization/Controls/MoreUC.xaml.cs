﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// Interaction logic for ExportUserControl.xaml
    /// </summary>
    public partial class MoreUC : UserControl
    {
        public MoreUC()
        {
            InitializeComponent();
        }

        public ImageSource IconSource
        {
            get { return (ImageSource)base.GetValue(IconSourceProperty); }
            set { base.SetValue(IconSourceProperty, value); }
        }
        public static readonly DependencyProperty IconSourceProperty = DependencyProperty.Register("IconSource", typeof(ImageSource), typeof(MoreUC), new PropertyMetadata(null));

        public object MenuContent
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty = DependencyProperty.Register("MenuContent", typeof(object), typeof(MoreUC), new PropertyMetadata(null));

        public void MoreSectionToggleVisibility(object sender, RoutedEventArgs e)
        {
            MoreSectionPopup.IsOpen = !MoreSectionPopup.IsOpen;
        }
    }
}
