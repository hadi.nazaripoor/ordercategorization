﻿using OrderCategorization.Asset.Attributes;
using OrderCategorization.DataAccess.Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq.Expressions;
using OrderCategorization.Asset;

namespace OrderCategorization.Controls
{
    /// <summary>
    /// An Editable ComboBox With Suggestion
    /// </summary>
    public partial class ComboBoxUC : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        public PropertyInfo KeyProperty { get; set; }
        public PropertyInfo Title_Property { get; set; }
        public List<PropertyInfo> ComboBoxSearchProperties { get; set; }
        public bool IsKeyboardLocked
        {
            get { return SuggestionGrid.IsOpen; }
        }
        public bool WasDoubleClickForSelectItem { get; set; } = false;

        public event EventHandler<Model> SelectionChanged;
        public event EventHandler<Model> SelectionFinalized;

        public string Input
        {
            get { return (string)base.GetValue(InputProperty); }
            set { base.SetValue(InputProperty, value); NotifyPropertyChanged(nameof(Input)); }
        }
        public static readonly DependencyProperty InputProperty = DependencyProperty.Register("Input", typeof(string), typeof(ComboBoxUC), new PropertyMetadata(""));

        //public Visibility AddButtonVisibility
        //{
        //    get { return (Visibility)base.GetValue(AddButtonVisibilityProperty); }
        //    set { base.SetValue(AddButtonVisibilityProperty, value); }
        //}
        //public static readonly DependencyProperty AddButtonVisibilityProperty = DependencyProperty.Register("AddButtonVisibility", typeof(Visibility), typeof(ComboBoxUC), new PropertyMetadata(Visibility.Collapsed));

        public Type CreatePageType
        {
            get { return (Type)base.GetValue(CreatePageTypeProperty); }
            set { base.SetValue(CreatePageTypeProperty, value); NotifyPropertyChanged(nameof(CreatePageType)); }
        }
        public static readonly DependencyProperty CreatePageTypeProperty = DependencyProperty.Register("CreatePageType", typeof(Type), typeof(ComboBoxUC), new PropertyMetadata(null));

        public ComboBoxUC()
        {
            InitializeComponent();
        }

        public object SelectedValue
        {
            get { return (object)GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }
        public static readonly DependencyProperty SelectedValueProperty = DependencyProperty.Register("SelectedValue", typeof(object), typeof(ComboBoxUC), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ChangeInputText));

        private static void ChangeInputText(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var userControl = (ComboBoxUC)d;

            if (userControl.IsEnabled)
            {
                userControl.IsEnabled = false;
                if (userControl.ItemsSource != null && userControl.SelectedValue != null)
                {
                    var selectedItem = userControl.ItemsSource.FirstOrDefault(x => userControl.KeyProperty.GetValue(x).ToString() == userControl.SelectedValue.ToString());
                    userControl.Input = selectedItem != null ? $"{userControl.Title_Property.GetValue(selectedItem)}" : "";
                }
                userControl.IsEnabled = true;

                userControl.SelectionChanged?.Invoke(userControl, (Model)userControl.ResultListBox.SelectedItem);
            }
        }

        public ObservableCollection<Model> ItemsSource
        {
            get { return (ObservableCollection<Model>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); SetProperties(); }
        }
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<Model>), typeof(ComboBoxUC), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ChangeItemsSource));

        private static void ChangeItemsSource(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var userControl = (ComboBoxUC)d;
            userControl.SetProperties();
        }

        public void SetProperties()
        {
            IsEnabled = ItemsSource != null && ItemsSource.Count > 0;

            if (IsEnabled)
            {
                var itemType = ItemsSource.FirstOrDefault().GetType();
                ModelAttribute modelAttribute = itemType.GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

                ResultListBox.SelectedValuePath = modelAttribute.KeyPropertyName;
                ResultListBox.DisplayMemberPath = modelAttribute.TitlePropertyName;

                KeyProperty = itemType.GetProperty(modelAttribute.KeyPropertyName);
                Title_Property = itemType.GetProperty(modelAttribute.TitlePropertyName);

                if (!string.IsNullOrEmpty(modelAttribute.ComboBoxSearchPropertiesNames))
                {
                    ComboBoxSearchProperties = new List<PropertyInfo>();
                    var propertiesNames = modelAttribute.ComboBoxSearchPropertiesNames.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var propertyName in propertiesNames)
                        ComboBoxSearchProperties.Add(itemType.GetProperty(propertyName));
                }

                ResultListBox.ItemsSource = Where();
            }
        }

        private IEnumerable Where()
        {
            if (ComboBoxSearchProperties != null && ComboBoxSearchProperties.Count > 0)
            {
                IQueryable<Model> query = ItemsSource.Where(x => true).AsQueryable();

                var result = ItemsSource.ToList();
                Expression<Func<Model, bool>> whereClause = x => false;

                foreach (var property in ComboBoxSearchProperties)
                {
                    whereClause = whereClause.Or(x => property.GetValue(x) != null && property.GetValue(x).ToString().Contains(Input));
                }

                return query.Where(whereClause).ToList();
            }

            return ItemsSource.Where(x => Title_Property.GetValue(x).ToString().Contains(Input)).ToList();
        }

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.StaysOpen = false;
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.StaysOpen = true;
        }

        private void InputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsEnabled)
            {
                IsEnabled = false;

                ResultListBox.ItemsSource = Where();

                if (ResultListBox.Items.Count > 0 && ResultListBox.SelectedIndex == -1)
                {
                    ResultListBox.SelectedIndex = 0;
                    SelectionChanged?.Invoke(this, (Model)ResultListBox.SelectedItem);
                }

                SuggestionGrid.IsOpen = true;

                IsEnabled = true;
            }
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Down:
                    ResultListBox.SelectedIndex++;
                    SuggestionGrid.IsOpen = true;
                    break;
                case Key.Up:
                    if (ResultListBox.SelectedIndex != -1)
                        ResultListBox.SelectedIndex--;

                    SuggestionGrid.IsOpen = true;
                    break;
                case Key.Enter:
                    if (SuggestionGrid.IsOpen)
                    {
                        if (ResultListBox.SelectedIndex != -1)
                        {
                            object selectedItem = ResultListBox.SelectedItem;

                            InputTextBox.Text = (string)Title_Property.GetValue(selectedItem);
                            InputTextBox.Select(InputTextBox.Text.Length, 0);

                            SuggestionGrid.IsOpen = false;
                            SelectionChanged?.Invoke(this, (Model)ResultListBox.SelectedItem);
                            SelectionFinalized?.Invoke(this, (Model)ResultListBox.SelectedItem);
                        }
                        e.Handled = true;
                    }
                    break;
                case Key.Escape:
                    if (SuggestionGrid.IsOpen)
                    {
                        ResultListBox.SelectedIndex = -1;
                        Input = "";

                        SuggestionGrid.IsOpen = false;
                        e.Handled = true;
                    }
                    break;
                default:
                    break;
            }
        }

        private void ListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            object selectedItem = ResultListBox.SelectedItem;

            InputTextBox.Text = (string)Title_Property.GetValue(selectedItem);
            InputTextBox.Select(InputTextBox.Text.Length, 0);

            SuggestionGrid.IsOpen = false;
            SelectionChanged?.Invoke(this, (Model)ResultListBox.SelectedItem);
            SelectionFinalized?.Invoke(this, (Model)ResultListBox.SelectedItem);

            WasDoubleClickForSelectItem = true;
        }

        private void ResultListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ResultListBox.ScrollIntoView(ResultListBox.SelectedItem);
        }

        private void PopupToggle(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.IsOpen = !SuggestionGrid.IsOpen;
        }
    }
}
