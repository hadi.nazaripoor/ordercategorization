﻿using OrderCategorization.Asset.Enums;
using OrderCategorization.Asset.HelperModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace OrderCategorization.Views
{
    /// <summary>
    /// Interaction logic for DialogePage.xaml
    /// </summary>
    public partial class DialogePage : PageFunction<short>
    {
        KeyEventHandler WindowPreviewKeyDownEventHandler;
        Button FirstChoiceButton;

        public DialogePageModel DialogePageModel { get; set; }
        public DialogePage(DialogePageModel pageModel)
        {
            InitializeComponent();
            //
            DialogePageModel = pageModel;

            this.DataContext = DialogePageModel;

            WindowPreviewKeyDownEventHandler = (newS, newE) =>
            {
                if (newE.Key == Key.Enter) {if(FirstChoiceButton != null) DialogeResult_Click(FirstChoiceButton, null); newE.Handled = true; }
                else if (newE.Key == Key.Escape) { CloseButton_Click(null, null); newE.Handled = true; }
            };

            this.Loaded += (s, e) => { (App.Current.MainWindow).PreviewKeyDown += WindowPreviewKeyDownEventHandler; };
            this.Unloaded += (s, e) => { (App.Current.MainWindow).PreviewKeyDown -= WindowPreviewKeyDownEventHandler; };
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            int counter = 0;
            foreach (var buttonContent in DialogePageModel.ButtonsContent)
            {
                ResultButtonsContainerGrid.ColumnDefinitions.Add(new ColumnDefinition());

                var newButton = new Button { Style = FindResource("ConfirmButtonStyle") as Style, Content = buttonContent.Content, Tag = buttonContent.DialogeResultEnum, Margin = new Thickness(4,0,4,0) };
                newButton.SetValue(Grid.ColumnProperty, counter++);
                newButton.Click += DialogeResult_Click;

                ResultButtonsContainerGrid.Children.Add(newButton);
            }

            if (ResultButtonsContainerGrid.Children != null)
                FirstChoiceButton = ResultButtonsContainerGrid.Children[0] as Button;
        }

        private void DialogeResult_Click(object sender, RoutedEventArgs e)
        {
            var dialogeResult = (short)((DialogePageTypes)(((Button)sender).Tag));
            OnReturn(new ReturnEventArgs<short>(dialogeResult));
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            OnReturn(new ReturnEventArgs<short>(0));
        }
    }
}
