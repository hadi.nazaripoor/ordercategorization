﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Controls;
using OrderCategorization.DataAccess.Model;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace OrderCategorization.Views.CUs
{
    public class CreateUpdateBasePage<T> : PageFunction<int>, INotifyPropertyChanged where T : Model
    {
        #region NotifyPropertyChanged, Dispose
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        
        KeyEventHandler WindowPreviewKeyDownEventHandler;
        private bool _isTemplateForExcel;
        public bool IsTemplateForExcel
        {
            get { return _isTemplateForExcel; }
            set
            {
                _isTemplateForExcel = value;

                if (IsTemplateForExcel)
                {
                    PageTitle = $"قالب پیشفرض {ModelAttribute.SingleEntityTitle} برای ورودی اکسل";
                    StayOpenOrHasTitleRecordCheckBoxContent = "فایل ورودی شامل سطر عنوان است ؟";
                }
            }
        }

        public bool IsNew { get; set; }
        public bool StayOpenOrHasTitleRecord { get; set; }
        public string StayOpenOrHasTitleRecordCheckBoxContent { get; set; }
        public string PageTitle { get; set; }
        public Type RepositoryType { get; set; }
        public T PageModel { get; set; }
        public ObservableCollection<T> AffectedEntities { get; set; }
        ModelAttribute ModelAttribute = typeof(T).GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

        public CreateUpdateBasePage()
        {
            var keyboardUtil = new KeyboardUtil();
            var window = App.Current.MainWindow as MainWindow;

            WindowPreviewKeyDownEventHandler = (newS, newE) => { keyboardUtil.CreateUpdateBasePage_PreviewKeyDown(newE, this); };
            this.Loaded += (s, e) => { window.PreviewKeyDown += WindowPreviewKeyDownEventHandler; };
            this.Unloaded += (s, e) => { window.PreviewKeyDown -= WindowPreviewKeyDownEventHandler; };
        }

        protected void Initialize(long modelKeyId = 0, bool containsDeleted = false)
        {
            IsNew = modelKeyId == 0;
            PageTitle = IsNew ? $"{ModelAttribute.SingleEntityTitle} جدید" : $"ویرایش {ModelAttribute.SingleEntityTitle}";

            if (IsNew)
                StayOpenOrHasTitleRecordCheckBoxContent = $"صفحه برای وارد کردن {ModelAttribute.SingleEntityTitle} جدید باز بماند ؟";

            if (modelKeyId == 0)
                PageModel = (T)Activator.CreateInstance(typeof(T));
            else
                PageModel = GetSpecial(modelKeyId, containsDeleted);

            SelectFirstEntryControl();
        }

        protected T GetSpecial(long modelKeyId, bool containsDeleted = false, bool doCloning = false)
        {
            T model;

            var getSpecialMethod = RepositoryType.GetMethod("GetSpecial");

            if (getSpecialMethod.GetParameters().Count() == 1)
                model = (T)getSpecialMethod.Invoke(RepositoryType, new object[] { modelKeyId });
            else
                model = (T)getSpecialMethod.Invoke(RepositoryType, new object[] { modelKeyId, containsDeleted });

            if (doCloning)
                return DeepCloneUtil.Clone<T>(model, true);
            else
                return model;
        }

        protected void ClearContent()
        {
            PageModel = (T)Activator.CreateInstance(typeof(T));
            NotifyPropertyChanged(nameof(PageModel));

            SelectFirstEntryControl();
        }

        private void SelectFirstEntryControl()
        {
            var firstEntryControl = (Control)FindName("FirstEntryControl");
            if (firstEntryControl != null)
                firstEntryControl.Focus();
        }

        public void AddOrUpdate()
        {
            if (!PageModel.HasErrors)
            {
                if (AffectedEntities == null)
                    AffectedEntities = new ObservableCollection<T>();

                if (!IsTemplateForExcel)
                {
                    var addOrUpdtaeMethod = RepositoryType.GetMethod("AddOrUpdate");
                    var param = new object[] { PageModel };

                    addOrUpdtaeMethod.Invoke(RepositoryType, param);

                    var keyProperty = typeof(T).GetProperty(ModelAttribute.KeyPropertyName);
                    long modelKeyId = Convert.ToInt64(keyProperty.GetValue(PageModel));

                    var savedEntity = GetSpecial(modelKeyId);
                    AffectedEntities.Add(savedEntity);
                }
                else
                    AffectedEntities.Add(DeepCloneUtil.Clone<T>(PageModel, true));

                if (!StayOpenOrHasTitleRecord || IsTemplateForExcel)
                    this.OnReturn(new ReturnEventArgs<int>(1));
                else
                    ClearContent();
            }
        }

        private IList TargetIList;
        public void AddNewItem_Clicked(object sender, MouseButtonEventArgs e)
        {
            if (sender is ComboBoxUC && (sender as ComboBoxUC).WasDoubleClickForSelectItem)
            {
                (sender as ComboBoxUC).WasDoubleClickForSelectItem = false;
                return;
            }

            this.KeepAlive = true;
            dynamic senderAsExtendedComboBoxOrComboBoxUC = sender;
            var CUPageForCreateNewItem = Activator.CreateInstance(senderAsExtendedComboBoxOrComboBoxUC.CreatePageType);

            (CUPageForCreateNewItem as PageFunction<int>).Return += CUPageForCreateNewItem_Return;
            TargetIList = (IList)senderAsExtendedComboBoxOrComboBoxUC.ItemsSource;
            NavigationService.Navigate(CUPageForCreateNewItem);
        }

        private void CUPageForCreateNewItem_Return(object sender, ReturnEventArgs<int> e)
        {
            this.KeepAlive = false;
            dynamic dynamicCUPage = sender;

            if (dynamicCUPage.AffectedEntities != null)
                foreach (var newItem in dynamicCUPage.AffectedEntities)
                    TargetIList.Add(newItem);
        }

        public void Close(object sender, RoutedEventArgs e)
        {
            OnReturn(new ReturnEventArgs<int>(0));
        }
    }
}
