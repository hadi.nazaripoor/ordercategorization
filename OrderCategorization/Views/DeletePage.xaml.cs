﻿using System.Windows;
using System.Windows.Navigation;

namespace OrderCategorization.Views
{
    public partial class DeletePage : PageFunction<bool>
    {
        public string PageTitle { get; set; }
        public string WarningText { get; set; }
        public bool RealDelete { get; internal set; }

        public DeletePage()
        {
            InitializeComponent();
            //
            this.DataContext = this;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            OnReturn(new ReturnEventArgs<bool>(false));
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            OnReturn(new ReturnEventArgs<bool>(true));
        }
    }
}
