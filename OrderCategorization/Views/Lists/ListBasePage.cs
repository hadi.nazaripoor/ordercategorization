﻿using OrderCategorization.DataAccess.Model;
using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Navigation;
using OrderCategorization.Views.CUs;
using System.ComponentModel;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.Enums;
using OrderCategorization.Controls.EventArguments;
using OrderCategorization.Asset.HelperModels;
using OrderCategorization.Asset;
using OrderCategorization.Asset.Converters;
using System.Windows.Input;
using OrderCategorization.DataAccess.EF;

namespace OrderCategorization.Views.Lists
{
    public class ListBasePage<T> : Page, INotifyPropertyChanged where T : Model
    {
        #region NotifyPropertyChanged, Dispose
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        KeyEventHandler WindowPreviewKeyDownEventHandler;
        public EventHandler<EntityIdEventArg> UpdateCompleted;

        private T ExcelTemplate { get; set; }
        public string PageTitle { get; set; }
        public Type RepositoryType { get; set; }
        public Type CUPageType { get; set; }
        public CreateUpdateBasePage<T> CUPage { get; set; }
        public ObservableCollection<T> AllList { get; set; }
        public ObservableCollection<T> PageList { get; set; }
        public PropertyInfo KeyProperty { get; set; }
        public PropertyInfo Title_Property { get; set; }
        public PropertyInfo OrderProperty { get; set; }
        public bool IsOrderAsc { get; set; }
        public PropertyInfo TransactionId_Property { get; set; }
        public bool IsFirstEntityPermanent { get; set; }
        public bool HasTotalRow { get; set; }
        protected T SearchPrimaryModel { get; set; } = null;

        public long TempIdForDelete { get; set; }

        private SelectionType _selectionType = SelectionType.NonSelected;
        public SelectionType SelectionType
        {
            get { return _selectionType; }
            set { _selectionType = value; NotifyPropertyChanged(nameof(SelectionType)); }
        }
        public DeletePageData DeletePageData { get; set; }
        public ModelAttribute ModelAttribute { get; set; }
        public ListBasePage()
        {
            ModelAttribute = typeof(T).GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

            KeyProperty = typeof(T).GetProperty(ModelAttribute.KeyPropertyName);
            Title_Property = typeof(T).GetProperty(ModelAttribute.TitlePropertyName);
            if (ModelAttribute.HasTransaction)
                TransactionId_Property = typeof(T).GetProperty("TransactionId");

            IsFirstEntityPermanent = ModelAttribute.IsFirstEntityPermanent;
            HasTotalRow = ModelAttribute.HasTotalRow;

            PageTitle = $"لیست {ModelAttribute.MultipleEntitiesTitle}";
            
            if (!string.IsNullOrWhiteSpace(ModelAttribute.OrderPropertyName))
            {
                OrderProperty = typeof(T).GetProperty(ModelAttribute.OrderPropertyName);
                IsOrderAsc = ModelAttribute.IsOrderDesc;
            }
            
            var keyboardUtil = new KeyboardUtil();
            var window = App.Current.MainWindow as MainWindow;

            WindowPreviewKeyDownEventHandler = (newS, newE) => { keyboardUtil.ListBasePage_PreviewKeyDown(newE, this); };
            this.Loaded += (s, e) => { window.PreviewKeyDown += WindowPreviewKeyDownEventHandler; };
            this.Unloaded += (s, e) => { window.PreviewKeyDown -= WindowPreviewKeyDownEventHandler; };
        }

        protected void Initialize(bool containsDeleted = false)
        {
            var getAllMethod = RepositoryType.GetMethod("GetAll");

            if (getAllMethod.GetParameters().Count() > 0)
                AllList = (ObservableCollection<T>)getAllMethod.Invoke(RepositoryType, new object[] { containsDeleted }); // containsDeleted
            else
                AllList = (ObservableCollection<T>)getAllMethod.Invoke(RepositoryType, null);
            
            RefreshPageTitle();
            AllList.CollectionChanged += (s, e) =>
            {
                RefreshPageTitle();
            };
            
            if (IsFirstEntityPermanent && AllList.Count > 0)
                AllList.RemoveAt(0);

            if (HasTotalRow)
            {
                var totalEntry = (T)Activator.CreateInstance(typeof(T));
                totalEntry.IsTotalRow = true;
                AllList.Add(totalEntry);
            }

            PageList = new ObservableCollection<T>();

            foreach (var item in AllList)
            {
                PageList.Add(item);
            }

            PageList.CollectionChanged += PageList_CollectionChanged;
            PageList_CollectionChanged(null, null);
        }
        private void RefreshPageTitle()
        {
            var minusValueForTotalRow = HasTotalRow ? 1 : 0;
            PageTitle = $"لیست {ModelAttribute.MultipleEntitiesTitle} ( {(AllList.Count - minusValueForTotalRow).ToString("n0")} {ModelAttribute.SingleEntityTitle} )";
            NotifyPropertyChanged(nameof(PageTitle));
        }

        private void PageList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (PageList.Count == AllList.Count)
                PageList.SetModelOrderProperty();
        }

        public void AddNew(object sender, RoutedEventArgs e)
        {
            this.KeepAlive = true;
            CUPage = (CreateUpdateBasePage<T>)Activator.CreateInstance(CUPageType);

            CUPage.Return += CreatePage_Return;
            NavigationService.Navigate(CUPage);
        }

        protected void CreatePage_Return(object sender, ReturnEventArgs<int> e)
        {
            this.KeepAlive = false;

            var senderPage = sender as CreateUpdateBasePage<T>;

            if (senderPage.AffectedEntities != null)
            {
                foreach (T newItem in senderPage.AffectedEntities)
                {
                    if (HasTotalRow)
                    {
                        AllList.Insert(AllList.Count - 1, newItem);
                        PageList.Insert(PageList.Count - 1, newItem);
                    }
                    else
                    {
                        AllList.Add(newItem);
                        PageList.Add(newItem);
                    }
                }
            }

            senderPage = null;
        }

        public void Update(object sender, EntityIdEventArg e)
        {
            this.KeepAlive = true;

            dynamic selectedId = Convert.ChangeType(e.EntityId, KeyProperty.PropertyType);
            CUPage = (CreateUpdateBasePage<T>)Activator.CreateInstance(CUPageType, selectedId);

            CUPage.Return += UpdatePage_Return;
            NavigationService.Navigate(CUPage);
        }

        private void UpdatePage_Return(object sender, ReturnEventArgs<int> e)
        {
            this.KeepAlive = false;

            var senderPage = sender as CreateUpdateBasePage<T>;

            if (senderPage.AffectedEntities != null) // Update Done
            {
                var updatedItem = senderPage.AffectedEntities[0];

                object idAsObject = KeyProperty.GetValue(updatedItem);

                for (int i = 0; i < AllList.Count; i++)
                {
                    if (Convert.ToInt64(KeyProperty.GetValue(AllList[i])) == Convert.ToInt64(idAsObject))
                    {
                        updatedItem.IsSelected = AllList[i].IsSelected;

                        AllList[i] = updatedItem;
                        break;
                    }
                }

                for (int i = 0; i < PageList.Count; i++)
                {
                    if (Convert.ToInt64(KeyProperty.GetValue(PageList[i])) == Convert.ToInt64(idAsObject))
                    {
                        updatedItem.IsSelected = PageList[i].IsSelected;

                        PageList[i] = updatedItem;
                        break;
                    }
                }
            }

            senderPage = null;
        }

        public void Delete(object sender, EntityIdEventArg e)
        {
            this.KeepAlive = true;

            TempIdForDelete = e.EntityId;
            var deletePage = new DeletePage();

            T itemToDelete = PageList.FirstOrDefault(x => Convert.ToInt64(KeyProperty.GetValue(x)) == TempIdForDelete);

            var itemToDeleteTitle = Title_Property.GetValue(itemToDelete);

            deletePage.PageTitle = string.Format(DeletePageData.DeletePageTitle, "یک");
            deletePage.WarningText = string.Format(DeletePageData.SingleItemDeletionDescription, itemToDeleteTitle);

            deletePage.RealDelete = DeletePageData.RealDelete;
            deletePage.Return += DeletePage_Return;

            this.KeepAlive = true;

            NavigationService.Navigate(deletePage);
        }

        public void Delete(object sender, RoutedEventArgs e)
        {
            var itemsToDeleteCount = PageList.Where(x => x.IsSelected).Count();

            if (itemsToDeleteCount == 0)
                return;

            var deletePage = new DeletePage();

            if (itemsToDeleteCount == 1)
            {
                T itemToDelete = PageList.FirstOrDefault(x => x.IsSelected);

                var itemToDeleteTitle = Title_Property.GetValue(itemToDelete);

                deletePage.PageTitle = string.Format(DeletePageData.DeletePageTitle, "یک");
                deletePage.WarningText = string.Format(DeletePageData.SingleItemDeletionDescription, itemToDeleteTitle);
            }
            else
            {
                deletePage.PageTitle = string.Format(DeletePageData.DeletePageTitle, "چند");
                deletePage.WarningText = string.Format(DeletePageData.MultipleItemDeletionDescription, itemsToDeleteCount.ToString());
            }

            deletePage.RealDelete = DeletePageData.RealDelete;
            deletePage.Return += DeletePage_Return;

            this.KeepAlive = true;

            NavigationService.Navigate(deletePage);
        }

        private void DeletePage_Return(object sender, ReturnEventArgs<bool> e)
        {
            this.KeepAlive = false;

            if (!e.Result) { TempIdForDelete = 0; return; }

            var idsToDelete = new HashSet<long>();

            if (TempIdForDelete == 0)
                foreach (var selectedItem in PageList.Where(x => x.IsSelected))
                {
                    object idAsObject = KeyProperty.GetValue(selectedItem);
                    idsToDelete.Add(Convert.ToInt64(idAsObject));
                }
            else
                idsToDelete.Add(TempIdForDelete);

            var deleteMethod = RepositoryType.GetMethod("Delete");

            if (deleteMethod.GetParameters().Count() > 1)
                deleteMethod.Invoke(RepositoryType, new object[] { idsToDelete, (sender as DeletePage).RealDelete }); // realDelete
            else
                deleteMethod.Invoke(RepositoryType, new object[] { idsToDelete });

            for (int i = 0; i < AllList.Count; i++)
            {
                if (idsToDelete.Contains(Convert.ToInt64(KeyProperty.GetValue(AllList[i]))))
                {
                    AllList.RemoveAt(i);
                    i--;
                }

                TempIdForDelete = 0;
            }

            for (int i = 0; i < PageList.Count; i++)
            {
                if (idsToDelete.Contains(Convert.ToInt64(KeyProperty.GetValue(PageList[i]))))
                {
                    PageList.RemoveAt(i);
                    i--;
                }
            }
        }

        protected void SelectDeselectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedId = Convert.ToInt64((sender as Button).Tag);
            Model item = PageList.FirstOrDefault(x => Convert.ToInt64(KeyProperty.GetValue(x)) == selectedId);

            item.IsSelected = !item.IsSelected;

            if (PageList.Count > 1 + (HasTotalRow ? 1 : 0) && SelectionType != SelectionType.SubSectionSelected)
                SelectionType = SelectionType.SubSectionSelected;
            else
            {
                var findOpposite = false;

                foreach (var model in PageList)
                {
                    if (!model.IsTotalRow && model.IsSelected != item.IsSelected)
                    {
                        findOpposite = true;
                        break;
                    }
                }

                if (findOpposite)
                    SelectionType = SelectionType.SubSectionSelected;
                else
                    SelectionType = item.IsSelected ? SelectionType.AllSelected : SelectionType.NonSelected;
            }
        }

        public void AllSelectDeselectButton_Click(object sender, RoutedEventArgs e)
        {
            SelectionType = (SelectionType == SelectionType.AllSelected) ? SelectionType.NonSelected : SelectionType.AllSelected;

            foreach (var item in PageList)
                item.IsSelected = SelectionType == SelectionType.AllSelected;
        }

        public void Print()
        {

        }

        private void Export(object sender, OutputEventArg e)
        {
            //var resultResourceKeyNameToDisplayDialoge = ResourceKeyNames.None;

            //var modelToExportId = ExportPageTypeToTypeConverter.GetShortValue(typeof(T).Name);
            //if (modelToExportId != 0)
            //{
            //    switch (e.ImportExportTypeEnum)
            //    {
            //        case ImportExportTypes.PrintExport:
            //            resultResourceKeyNameToDisplayDialoge = WordUtil.DisplayOrPrintLists(modelToExportId, PageList, WordOperationType.Print);
            //            break;
            //        case ImportExportTypes.WordExport:
            //            resultResourceKeyNameToDisplayDialoge = WordUtil.DisplayOrPrintLists(modelToExportId, PageList);
            //            break;
            //        case ImportExportTypes.PDFExport:
            //            resultResourceKeyNameToDisplayDialoge = WordUtil.DisplayOrPrintLists(modelToExportId, PageList, WordOperationType.CreatePdfDocument);
            //            break;
            //        case ImportExportTypes.ExcelExport:
            //            resultResourceKeyNameToDisplayDialoge = ExcelUtil<T>.DisplayInExcel(modelToExportId, PageList);
            //            break;
            //        case ImportExportTypes.ExportSettings:
            //            NavigationService.Navigate(new ExportSettingsPage(modelToExportId));
            //            break;
            //        case ImportExportTypes.ExcelImport:
            //            {
            //                this.KeepAlive = true;
            //                CUPage = (CreateUpdateBasePage<T>)Activator.CreateInstance(CUPageType);
            //                CUPage.IsTemplateForExcel = true;

            //                CUPage.Return += CreateExcelTemplatePage_Return;
            //                NavigationService.Navigate(CUPage);
            //            }
            //            break;
            //        default:
            //            break;
            //    }

            //    if (resultResourceKeyNameToDisplayDialoge != ResourceKeyNames.None)
            //    {
            //        var dialogModel = FindResource(resultResourceKeyNameToDisplayDialoge.ToString()) as DialogePageModel;

            //        this.KeepAlive = true;

            //        var messagePage = new DialogePage(dialogModel);
            //        NavigationService.Navigate(messagePage);

            //        return;
            //    }
            //}
        }

        private void CreateExcelTemplatePage_Return(object sender, ReturnEventArgs<int> e)
        {
            this.KeepAlive = false;

            var senderPage = sender as CreateUpdateBasePage<T>;

            if (senderPage.AffectedEntities != null && senderPage.AffectedEntities.Count == 1)
            {
                ExcelTemplate = senderPage.AffectedEntities[0];
                //
                var excelFileCollection = ExcelUtil<T>.ReadFileContent(ExcelTemplate, senderPage.StayOpenOrHasTitleRecord);

                if (excelFileCollection.Count == 0)
                {
                    var dialogModel = FindResource(ResourceKeyNames.NoDataFoundInExcelFileToImportDialogePageModel.ToString()) as DialogePageModel;
                    var messagePage = new DialogePage(dialogModel);

                    this.KeepAlive = true;
                    NavigationService.Navigate(messagePage);
                }
                else
                {
                    PageList.Clear();

                    foreach (var item in excelFileCollection)
                    {
                        PageList.Add(item);
                    }
                }
            }

            senderPage = null;
        }

        public void StartSearch(object sender, RoutedEventArgs e)
        {
            var mainWindow = (MainWindow)Application.Current.MainWindow;

            mainWindow.ShowSearchBarForModel<T>(AllList, PageList, SearchPrimaryModel);
        }

        public void Close(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
