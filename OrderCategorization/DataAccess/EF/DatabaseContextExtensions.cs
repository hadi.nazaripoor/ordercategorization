﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace OrderCategorization.DataAccess.EF
{
    public static class DatabaseContextExtensions
    {
        public static long GetRowsCount(this DatabaseContext dbContext, Type modelType)
        {
            var tableName = dbContext.GetTableName(modelType);
            var queryString = $"Select Count(*) From {tableName} As RowsCount";

            using (var sqlDataReader = DatabaseUtil.ExecuteReader(queryString))
            {
                if (sqlDataReader.Read())
                    return long.Parse(sqlDataReader[0].ToString());
            }

            return 0;
        }

        //public static List<T> FastSelect<T>(this DatabaseContext dbContext, params Type[] foreignTypes) where T : Model.Model
        //{
        //    var resultList = new List<T>();
        //    var newT = Activator.CreateInstance<T>();
        //    var tableName = dbContext.GetTableName<T>();
        //    var requiredProperties = new List<PropertyInfo>();

        //    foreach (PropertyInfo prop in newT.GetType().GetProperties())
        //    {
        //        var sqlFetchAttribute = prop.GetCustomAttribute(typeof(SqlFetchAttribute)) as SqlFetchAttribute;

        //        if (sqlFetchAttribute != null)
        //            requiredProperties.Add(prop);
        //    }

        //    var queryString = $"Select {string.Join(", ", requiredProperties.Select(p => p.Name))} From {tableName}";


        //    using (var sqlDataReader = DatabaseUtil.ExecuteReader(queryString))
        //    {

        //        while (sqlDataReader.Read())
        //        {
        //            newT = Activator.CreateInstance<T>();

        //            foreach (PropertyInfo prop in requiredProperties)
        //            {
        //                if (!object.Equals(sqlDataReader[prop.Name], DBNull.Value))
        //                {
        //                    prop.SetValue(newT, sqlDataReader[prop.Name]);
        //                }
        //            }

        //            resultList.Add(newT);
        //        }
        //    }

        //    return resultList;
        //}

        public static int BulkInsert<T>(this DatabaseContext dbContext, ObservableCollection<T> entities) where T : Model.Model
        {
            if (entities.Count > 0)
            {
                var tableName = dbContext.GetTableName<T>();
                var properties = typeof(T).GetProperties().Where(property => property.GetCustomAttribute(typeof(RequiredAttribute)) != null);
                var propertiesNames = string.Join(", ", properties.Select(property => property.Name));
                var stringBuilder = new StringBuilder($"Insert Into {tableName} ({propertiesNames}) Values ");

                for (int i = 0; i < entities.Count; i++)
                {
                    var entity = entities[i];
                    stringBuilder.Append("(");

                    for (int j = 0; j < properties.Count(); j++)
                    {
                        var property = properties.ElementAt(j);

                        if (property.PropertyType == typeof(string))
                            stringBuilder.Append("N");

                        stringBuilder.Append($"'{property.GetValue(entity).ToString()}'");

                        if (j < properties.Count() - 1) stringBuilder.Append(", ");
                    }

                    stringBuilder.Append(")");

                    if (i < entities.Count() - 1) stringBuilder.Append(",");
                }

                DatabaseUtil.ExecuteNonQuery(stringBuilder.ToString());
            }

            return entities.Count;
        }

        public static int BulkDelete<T>(this DatabaseContext dbContext, HashSet<long> idsToDeleteHashSet) where T : Model.Model
        {
            if (idsToDeleteHashSet.Count > 0)
            {
                var tableName = dbContext.GetTableName<T>();
                ModelAttribute modelAttribute = typeof(T).GetCustomAttribute(typeof(ModelAttribute)) as ModelAttribute;

                var separator = ",";

                var stringBuilder = new StringBuilder($"Delete From {tableName} Where {modelAttribute.KeyPropertyName} IN ({string.Join(separator, idsToDeleteHashSet)})");

                DatabaseUtil.ExecuteNonQuery(stringBuilder.ToString());
            }

            return idsToDeleteHashSet.Count;
        }

        private static string GetTableName<T>(this DatabaseContext dbContext) where T : Model.Model
        {
            ObjectContext objectContext = ((IObjectContextAdapter)dbContext).ObjectContext;

            return objectContext.GetTableName<T>();
        }

        private static string GetTableName<T>(this ObjectContext dbContext) where T : Model.Model
        {
            string sql = dbContext.CreateObjectSet<T>().ToTraceString();
            Regex regex = new Regex("FROM (?<table>.*) AS");
            Match match = regex.Match(sql);

            string table = match.Groups["table"].Value;
            return table;
        }

        public static string GetTableName(this DatabaseContext dbContext, Type entityType)
        {
            var sql = dbContext.Set(entityType).ToString();
            var regex = new Regex(@"FROM \[dbo\]\.\[(?<table>.*)\] AS");
            var match = regex.Match(sql);

            return match.Groups["table"].Value;
        }
    }
}
