﻿using OrderCategorization.Asset;
using OrderCategorization.DataAccess.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using System.Linq;
using OrderCategorization.Asset.Enums;
using System;

namespace OrderCategorization.DataAccess.EF
{
    public class ProductRepository
    {
        public static int AddOrUpdate(Product entity)
        {
            var entities = new ObservableCollection<Product>();
            entities.Add(entity);

            return BulkAddOrUpdate(entities);
        }

        public static int BulkAddOrUpdate(ObservableCollection<Product> entities)
        {
            using (var dbContext = new DatabaseContext())
            {
                dbContext.Products.AddOrUpdate(entities.ToArray());
                dbContext.SaveChanges();

                var entitiesIds = entities.Select(e => e.ProductId);
                var manipulatedEntities = dbContext.Products.Include(nameof(Unit)).Where(x => entitiesIds.Contains(x.ProductId)).ToList();

                CacheUtil.SyncData(manipulatedEntities, DataSyncType.CreateUpdate);

                return entities.Count;
            }
        }

        public static ObservableCollection<Product> GetAll(bool containsDeleted = false)
        {
            var allProduct = CacheUtil.Where<Product>(x => containsDeleted || !x.IsDeleted);
            foreach (var product in allProduct)
                product.FakeCount = 0;

            return GenericUtil.ToObservableCollection(allProduct);
        }

        public static void FillProductsIdAndNameDictionary(Dictionary<string, int> productsIdAndNameDictionary)
        {
            using (var dbContext = new DatabaseContext())
            {
                var productsNameAndIdList = dbContext.Products.Select(x => new { ProductId = x.ProductId, Name = x.Name }).ToList();

                foreach (var productNameAndId in productsNameAndIdList)
                    if (!productsIdAndNameDictionary.ContainsKey(productNameAndId.Name))
                        productsIdAndNameDictionary.Add(productNameAndId.Name, productNameAndId.ProductId);
            }
        }

        public static ObservableCollection<Product> GetAllWithDontCare(bool containsDeleted = false)
        {
            var all = GetAll(containsDeleted);
            var newEntry = new Product();

            GenericUtil.AddDontCareEntry(all, newEntry);

            return all;
        }

        public static Product GetSpecial(long keyId, bool containsDeleted = false)
        {
            return CacheUtil.GetFirstOrDefault<Product>(x => x.ProductId == keyId && (containsDeleted || !x.IsDeleted));
        }

        public static int Delete(HashSet<long> idsToDeleteHashSet, bool realDelete = false)
        {
            using (var dbContext = new DatabaseContext())
            {
                var list = dbContext.Products.Where(x => idsToDeleteHashSet.Contains(x.ProductId)).ToList();

                if (!realDelete)
                    foreach (var item in list)
                        item.IsDeleted = true;
                else
                    dbContext.Products.RemoveRange(list);

                if (realDelete)
                {
                    CacheUtil.SyncData(list, DataSyncType.RealDelete);
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.SaveChanges();
                    CacheUtil.SyncData(list, DataSyncType.CreateUpdate);
                }

                return list.Count();
            }
        }
    }
}
