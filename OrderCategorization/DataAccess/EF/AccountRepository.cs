﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using System.Linq;

namespace OrderCategorization.DataAccess.EF
{
    public class AccountRepository
    {
        public static int AddOrUpdate(Account entity)
        {
            var entities = new ObservableCollection<Account>();
            entities.Add(entity);
            var isNewAccount = entity.AccountId == 0;

            var saveResult = BulkAddOrUpdate(entities);

            //INTEGRITY
            //if (isNewAccount && entity.PrimaryBalance != 0)
            //    TransactionRepository.AddInitTransaction(TransactionCauserType.Account, entity.AccountId, entity.PrimaryBalance);

            return saveResult;
        }

        public static int BulkAddOrUpdate(ObservableCollection<Account> entities)
        {
            using (var dbContext = new DatabaseContext())
            {
                dbContext.Accounts.AddOrUpdate(entities.ToArray());
                dbContext.SaveChanges();

                var entitiesIds = entities.Select(e => e.AccountId);
                var manipulatedEntities = dbContext.Accounts.Where(x => entitiesIds.Contains(x.AccountId)).ToList();

                CacheUtil.SyncData(manipulatedEntities, DataSyncType.CreateUpdate);
                //CacheUtil.SyncData<Account>(x => entitiesIds.Contains(x.AccountId), DataSyncType.CreateUpdate);

                return entities.Count;
            }
        }

        public static ObservableCollection<Account> GetAll(bool containsDeleted = false)
        {
            return GenericUtil.ToObservableCollection(CacheUtil.Where<Account>(x => (containsDeleted || !x.IsDeleted)));
        }

        public static ObservableCollection<Account> GetAllWithDontCare(bool containsDeleted = false)
        {
            var all = GetAll(containsDeleted);
            var newEntry = new Account();

            GenericUtil.AddDontCareEntry(all, newEntry);

            return all;
        }

        public static ObservableCollection<Account> GetActiveAccounts(bool containsDeleted = false)
        {
            return GenericUtil.ToObservableCollection(CacheUtil.Where<Account>(x => (containsDeleted || !x.IsDeleted) && x.IsActive));
        }

        public static ObservableCollection<Account> GetByAccountType(bool isCash, bool containsDeleted = false)
        {
            return GenericUtil.ToObservableCollection(CacheUtil.Where<Account>(x => (containsDeleted || !x.IsDeleted) && x.IsCash == isCash));
        }

        public static Account GetSpecial(long keyId, bool containsDeleted = false)
        {
            return CacheUtil.GetFirstOrDefault<Account>(x => x.AccountId == keyId && (containsDeleted || !x.IsDeleted));
        }

        public static int Delete(HashSet<long> idsToDeleteHashSet, bool realDelete = false)
        {
            using (var dbContext = new DatabaseContext())
            {
                var list = dbContext.Accounts.Where(x => idsToDeleteHashSet.Contains(x.AccountId)).ToList();

                if (!realDelete)
                    foreach (var item in list)
                        item.IsDeleted = true;
                else
                    dbContext.Accounts.RemoveRange(list);

                if (realDelete)
                {
                    CacheUtil.SyncData<Account>(list, DataSyncType.RealDelete);
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.SaveChanges();
                    CacheUtil.SyncData<Account>(list, DataSyncType.CreateUpdate);
                }

                return list.Count();
            }
        }
    }
}
