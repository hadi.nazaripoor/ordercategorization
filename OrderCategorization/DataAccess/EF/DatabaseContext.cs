﻿using OrderCategorization.Asset;
using OrderCategorization.DataAccess.Model;
using OrderCategorization.Migrations;
using System.Data.Entity;

namespace OrderCategorization.DataAccess.EF
{
    public class DatabaseContext : DbContext
    {
        //public DatabaseContext() : base(DatabaseUtil.GetConnectionString())
        //{
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseContext, Configuration>());
        //}

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Shortcut> Shortcuts { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
