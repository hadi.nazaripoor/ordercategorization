﻿using OrderCategorization.DataAccess.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Migrations;
using OrderCategorization.Asset;
using OrderCategorization.Asset.Enums;

namespace OrderCategorization.DataAccess.EF
{
    public class UserRepository
    {
        public static int AddOrUpdate(User entity)
        {
            var entities = new ObservableCollection<User>();
            entities.Add(entity);

            return BulkAddOrUpdate(entities);
        }

        public static int BulkAddOrUpdate(ObservableCollection<User> entities)
        {
            using (var dbContext = new DatabaseContext())
            {
                dbContext.Users.AddOrUpdate(entities.ToArray());
                dbContext.SaveChanges();

                var entitiesIds = entities.Select(e => e.UserId);
                var manipulatedEntities = dbContext.Users.Include(nameof(Person)).Where(x => entitiesIds.Contains(x.UserId)).ToList();

                CacheUtil.SyncData(manipulatedEntities, DataSyncType.CreateUpdate);
                //CacheUtil.SyncData<User>(x => entitiesIds.Contains(x.UserId), DataSyncType.CreateUpdate);

                return entities.Count;
            }
        }

        public static ObservableCollection<User> GetAll(bool containsDeleted = false)
        {
            return GenericUtil.ToObservableCollection(CacheUtil.Where<User>(x => (containsDeleted || !x.IsDeleted)));
        }

        public static ObservableCollection<User> GetAllWithDontCare(bool containsDeleted = false)
        {
            var all = GetAll();
            var newEntry = new User();

            GenericUtil.AddDontCareEntry(all, newEntry);

            return all;
        }

        public static User GetSpecial(long keyId, bool containsDeleted = false)
        {
            return CacheUtil.GetFirstOrDefault<User>(x => x.UserId == keyId && (containsDeleted || !x.IsDeleted));
        }

        public static User GetSpecialByUsernamePassword(string username, string password, bool containsDeleted = false)
        {
            var hashedPassword = CryptographyUtil.GenerateSaltedHashBytes(password);

            return CacheUtil.GetFirstOrDefault<User>(x => x.Username == username && x.HashedPassword == hashedPassword);
        }

        public static int Delete(HashSet<long> idsToDeleteHashSet, bool realDelete = false)
        {
            using (var dbContext = new DatabaseContext())
            {
                var list = dbContext.Users.Where(x => idsToDeleteHashSet.Contains(x.UserId)).ToList();

                if (!realDelete)
                    foreach (var item in list)
                        item.IsDeleted = true;
                else
                    dbContext.Users.RemoveRange(list);

                if (realDelete)
                {
                    CacheUtil.SyncData(list, DataSyncType.RealDelete);
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.SaveChanges();
                    CacheUtil.SyncData(list, DataSyncType.CreateUpdate);
                }

                return list.Count();
            }
        }
    }
}
