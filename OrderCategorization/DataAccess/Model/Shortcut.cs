﻿using System;
using System.ComponentModel.DataAnnotations;
using OrderCategorization.Asset.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using OrderCategorization.Asset.HelperModels;
using System.Windows.Input;
using OrderCategorization.Asset.Enums;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = false, SingleEntityTitle = "کلید میانبر", MultipleEntitiesTitle = "کلیدهای میانبر", KeyPropertyName = nameof(ShortcutId), TitlePropertyName = nameof(PageOrOperationTypeTitle))]
    public class Shortcut : Model
    {
        private short _shortcutId;
        private short _pageOrOperationTypeEnum;
        private string _pageOrOperationTypeTitle;
        private short _modifierKeys;
        private short _key;
        private bool _isInQuickAccess;

        [Key]
        public short ShortcutId
        {
            get { return _shortcutId; }
            set { _shortcutId = value; NotifyPropertyChanged(nameof(ShortcutId)); }
        }

        [Required]
        public short PageOrOperationTypeEnum
        {
            get { return _pageOrOperationTypeEnum; }
            set { _pageOrOperationTypeEnum = value; NotifyPropertyChanged(nameof(PageOrOperationTypeEnum)); }
        }

        [NotMapped]
        public string PageOrOperationTypeTitle
        {
            get { return _pageOrOperationTypeTitle; }
            set { _pageOrOperationTypeTitle = value; NotifyPropertyChanged(nameof(PageOrOperationTypeTitle)); }
        }

        [Required]
        public short ModifierKeys
        {
            get { return _modifierKeys; }
            set { _modifierKeys = value; NotifyPropertyChanged(nameof(ModifierKeys)); }
        }

        [Required]
        public short Key
        {
            get { return _key; }
            set { _key = value; NotifyPropertyChanged(nameof(Key)); }
        }

        [Required]
        public bool IsInQuickAccess
        {
            get { return _isInQuickAccess; }
            set { _isInQuickAccess = value; NotifyPropertyChanged(nameof(IsInQuickAccess)); }
        }

        [NotMapped]
        public string ShortcutAsString
        {
            get{ return new ShortcutData { ModifierKeys = (ModifierKeys)this.ModifierKeys, Key = (Key)this.Key }.ShortcutOutput; }
        }

        public Shortcut()
        {
        }
    }
}
