﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using System.ComponentModel.DataAnnotations;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = false, SingleEntityTitle = "واحد شمارش", MultipleEntitiesTitle = "واحدهای شمارش", KeyPropertyName = nameof(UnitId), TitlePropertyName = nameof(Title))]
    public class Unit : Model
    {
        private short _unitId;
        private string _title;
        private string _stringFormat;
        private string _comment;

        [Key]
        public short UnitId
        {
            get { return _unitId; }
            set { _unitId = value; NotifyPropertyChanged(nameof(UnitId)); }
        }

        [Required(ErrorMessage = "عنوان واحد شمارش نمی تواند خالی باشد")]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        public string StringFormat
        {
            get { return _stringFormat; }
            set { _stringFormat = value; NotifyPropertyChanged(nameof(StringFormat)); }
        }

        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.VERY_LARGE_STRING_LENGTH)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; NotifyPropertyChanged(nameof(Comment)); }
        }

        public Unit()
        {
            StringFormat = "{0:n0}";
        }
    }
}
