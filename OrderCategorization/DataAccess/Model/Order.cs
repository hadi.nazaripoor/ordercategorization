﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.ObjectModel;
using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.DataAccess.EF;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = true, SingleEntityTitle = "خرید", MultipleEntitiesTitle = "خرید ها", KeyPropertyName = nameof(OrderId), TitlePropertyName = nameof(OrderId))]
    public class Order : Model
    {
        private int _orderId;
        private int _uniqueProductCount;
        private string _dateTime = PersianUtil.PersianDate();
        private string _comment;

        [Key]
        [Print(Title = "شماره سفارش")]
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; NotifyPropertyChanged(nameof(OrderId)); }
        }

        [NotMapped]
        public int UniqueProductCount
        {
            get { return _uniqueProductCount; }
            set { _uniqueProductCount = value; NotifyPropertyChanged(nameof(UniqueProductCount)); }
        }

        [Required]
        [Print(Title = "تاریخ سفارش", StringFormat = "Date")]
        [MaxLength(Constants.LENGTH_FOR_DATE_TIMES)]
        public string DateTime
        {
            get { return _dateTime; }
            set { _dateTime = value; NotifyPropertyChanged(nameof(DateTime)); }
        }

        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.VERY_LARGE_STRING_LENGTH)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; NotifyPropertyChanged(nameof(Comment)); }
        }

        public Order()
        {
            Comment = "";
        }
    }
}
