﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderCategorization.DataAccess.Model
{
    public class OrderDetail : Model
    {
        private long _orderDetailId;
        private int _orderId;
        private Order _order;
        private int _productId;
        private Product _product;

        private short _unitId;
        private Unit _unit;

        private double _totalCount;
        private string _totalCountWithUnit;

        [Key]
        [Print(Title = "ردیف")]
        public long OrderDetailId
        {
            get { return _orderDetailId; }
            set { _orderDetailId = value; NotifyPropertyChanged(nameof(OrderDetailId)); }
        }

        [Required]
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; NotifyPropertyChanged(nameof(OrderId)); }
        }

        [Required]
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; NotifyPropertyChanged(nameof(ProductId)); }
        }

        [ForeignKey(nameof(ProductId))]
        [Print(Title = "نام محصول یا خدمات")]
        public Product Product
        {
            get { return _product; }
            set { _product = value; NotifyPropertyChanged(nameof(Product)); }
        }

        [Required]
        public short UnitId
        {
            get { return _unitId; }
            set { _unitId = value; NotifyPropertyChanged(nameof(UnitId)); }
        }

        //[ForeignKey(nameof(UnitId))]
        //public Unit Unit
        //{
        //    get { return _unit; }
        //    set { _unit = value; NotifyPropertyChanged(nameof(Unit)); }
        //}

        [Print(Title = "تعداد", StringFormat = "{0:n0}")]
        [Required]
        public double TotalCount
        {
            get { return _totalCount; }
            set { _totalCount = value; NotifyPropertyChanged(nameof(TotalCount)); }
        }

        [NotMapped]
        public string TotalCountWithUnit
        {
            get { return _totalCountWithUnit; }
            set { _totalCountWithUnit = value; NotifyPropertyChanged(nameof(TotalCountWithUnit)); }
        }

        public OrderDetail()
        {
            TotalCount = 1;
        }
    }
}
