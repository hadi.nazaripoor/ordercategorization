﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.ObjectModel;
using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.EF;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = false, SingleEntityTitle = "کاربر", MultipleEntitiesTitle = "کاربران", KeyPropertyName = nameof(UserId), TitlePropertyName = nameof(Username))]
    public class User : Model
    {
        private short _userId;
        private string _username;
        private string _hashedPassword;

        private bool _isDeleted;

        [Key]
        public short UserId
        {
            get { return _userId; }
            set { _userId = value; NotifyPropertyChanged(nameof(UserId)); }
        }

        [Required(ErrorMessage = "نام کاربری نمی تواند خالی باشد")]
        [Search(Title = "نام کاربری", Description = "بخشی از نام کاربری را وارد کنید", DefaultValue = "", SearchControlEnum = SearchControlType.TextBox)]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string Username
        {
            get { return _username; }
            set { _username = value; NotifyPropertyChanged(nameof(Username)); }
        }

        [NotMapped]
        public string Password
        {
            get { return ""; } // Not Important
            set { HashedPassword = CryptographyUtil.GenerateSaltedHashBytes(value); }
        }

        [Required]
        [MaxLength(Constants.LARGE_STRING_LENGTH)]
        public string HashedPassword
        {
            get { return _hashedPassword; }
            set { _hashedPassword = value; NotifyPropertyChanged(nameof(HashedPassword)); }
        }

        [Required]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; NotifyPropertyChanged(nameof(IsDeleted)); }
        }

        public User()
        {
        }
    }
}
