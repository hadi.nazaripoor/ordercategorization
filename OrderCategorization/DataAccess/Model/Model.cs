﻿using OrderCategorization.Asset.Attributes;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace OrderCategorization.DataAccess.Model
{
    public abstract class Model : INotifyDataErrorInfo, IDisposable, INotifyPropertyChanged
    {
        #region DataErrorInfo,NotifyPropertyChanged, Dispose
        public ConcurrentDictionary<string, List<string>> _errors = new ConcurrentDictionary<string, List<string>>();

        public void AddError(string propertyName, string errorMessage)
        {
            if (_errors.ContainsKey(propertyName))
                _errors[nameof(propertyName)].Add(errorMessage);
            else
                _errors[propertyName] = new List<string> { errorMessage };

            OnErrorsChanged(propertyName);
        }

        public void RemoveError(string propertyName, string errorMessage)
        {
            if (_errors.ContainsKey(propertyName))
            {
                _errors[propertyName].Remove(errorMessage);

                if (_errors[propertyName].Count == 0)
                {
                    List<string> outLi;
                    _errors.TryRemove(propertyName, out outLi);
                    OnErrorsChanged(propertyName);
                }
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public void OnErrorsChanged(string propertyName)
        {
            var handler = ErrorsChanged;
            if (handler != null)
                handler(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errorsForName;
            _errors.TryGetValue(propertyName, out errorsForName);
            return errorsForName;
        }

        [NotMapped]
        public bool HasErrors
        {
            get { return _errors.Any(kv => kv.Value != null && kv.Value.Count > 0); }
            set { }
        }

        public Task ValidateAsync()
        {
            return Task.Run(() => Validate());
        }

        private object _lock = new object();
        public void Validate()
        {
            lock (_lock)
            {
                var validationContext = new ValidationContext(this, null, null);
                var validationResults = new List<ValidationResult>();
                Validator.TryValidateObject(this, validationContext, validationResults, true);

                foreach (var kv in _errors.ToList())
                {
                    if (validationResults.All(r => r.MemberNames.All(m => m != kv.Key)))
                    {
                        List<string> outLi;
                        _errors.TryRemove(kv.Key, out outLi);
                        OnErrorsChanged(kv.Key);
                    }
                }

                var q = from r in validationResults
                        from m in r.MemberNames
                        group r by m into g
                        select g;

                foreach (var prop in q)
                {
                    var messages = prop.Select(r => r.ErrorMessage).ToList();

                    if (_errors.ContainsKey(prop.Key))
                    {
                        List<string> outLi;
                        _errors.TryRemove(prop.Key, out outLi);
                    }
                    _errors.TryAdd(prop.Key, messages);
                    OnErrorsChanged(prop.Key);
                }
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            ValidateAsync().Wait();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {

        }
        #endregion

        private long _order;
        private bool _isTotalRow = false;
        private bool _isSelected = false;
        private Visibility _visibility = Visibility.Visible;

        public static bool _propagateVisibilityNotification;

        [NotMapped]
        [Print(Title = "ردیف")]
        public long Order
        {
            get { return _order; }
            set { _order = value; NotifyPropertyChanged(nameof(Order)); }
        }

        [NotMapped]
        public bool IsTotalRow
        {
            get { return _isTotalRow; }
            set { _isTotalRow = value; NotifyPropertyChanged(nameof(IsTotalRow)); }
        }

        [NotMapped]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; NotifyPropertyChanged(nameof(IsSelected)); }
        }

        [NotMapped]
        public Visibility Visibility
        {
            get { return _visibility; }
            set { _visibility = value; NotifyPropertyChanged(nameof(Visibility)); }
        }
    }
}