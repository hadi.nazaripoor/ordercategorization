﻿using System.ComponentModel.DataAnnotations;
using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.Enums;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = false, HasTotalRow = true, SingleEntityTitle = "حساب", MultipleEntitiesTitle = "حساب ها", KeyPropertyName = nameof(AccountId), TitlePropertyName = nameof(Title))]
    public class Account : Model
    {
        private short _accountId;
        private bool _isCash;
        private string _title;
        private string _accountNo;
        private string _cardNo;
        private string _comment;

        private long _primaryBalance;
        private long _balance;

        private bool _isActive;

        private bool _isDeleted;

        [Print(Title = "آیدی")]
        [Key]
        public short AccountId
        {
            get { return _accountId; }
            set { _accountId = value; NotifyPropertyChanged(nameof(AccountId)); }
        }

        [Print(Title = "عنوان حساب")]
        [Search(Title = "عنوان حساب", Description = "بخشی از عنوان حساب را وارد کنید", DefaultValue = "", SearchControlEnum = SearchControlType.TextBox)]
        [Total(TotalType = TotalAttributeType.Title, Title = "مجموع موجودی حساب ها")]
        [Required(ErrorMessage = "عنوان حساب نمی تواند خالی باشد")]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        [Required]
        public bool IsCash
        {
            get { return _isCash; }
            set { _isCash = value; NotifyPropertyChanged(nameof(IsCash)); }
        }

        [Print(Title = "شماره حساب")]
        [Search(Title = "شماره حساب", Description = "بخشی از شماره حساب را وارد کنید", DefaultValue = "", SearchControlEnum = SearchControlType.TextBox)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; NotifyPropertyChanged(nameof(AccountNo)); }
        }

        [Print(Title = "شماره کارت")]
        [Search(Title = "شماره کارت", Description = "بخشی از شماره کارت را وارد کنید", DefaultValue = "", SearchControlEnum = SearchControlType.TextBox)]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; NotifyPropertyChanged(nameof(CardNo)); }
        }

        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.VERY_LARGE_STRING_LENGTH)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; NotifyPropertyChanged(nameof(Comment)); }
        }

        [Required]
        public long PrimaryBalance
        {
            get { return _primaryBalance; }
            set { _primaryBalance = value; NotifyPropertyChanged(nameof(PrimaryBalance)); }
        }

        [Total(TotalType = TotalAttributeType.Sum)]
        [Print(Title = "موجودی", StringFormat = "{0:n0}")]
        [Required]
        public long Balance
        {
            get { return _balance; }
            set { _balance = value; NotifyPropertyChanged(nameof(Balance)); }
        }

        [Required]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; NotifyPropertyChanged(nameof(IsActive)); }
        }

        [Required]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; NotifyPropertyChanged(nameof(IsDeleted)); }
        }

        public Account()
        {
            PrimaryBalance = Balance = AccountId = 0;
            Title = AccountNo = CardNo = Comment = "";
            IsDeleted = false;
        }
    }
}
