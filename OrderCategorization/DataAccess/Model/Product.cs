﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using OrderCategorization.Asset.Enums;
using OrderCategorization.DataAccess.EF;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = false, SingleEntityTitle = "محصول", MultipleEntitiesTitle = "محصولات", KeyPropertyName = nameof(ProductId), TitlePropertyName = nameof(Name), OrderPropertyName = nameof(Name))]
    public class Product : Model
    {
        private int _productId;
        private string _name;
        private double _count;
        private double _fakeCount;
        private short _unitId;
        private Unit _unit;

        private string _uniqueIdentifier; // Maybe Barcode, RFID Id, etc.
        private string _comment;

        private bool _isDeleted;

        [Print(Title = "آیدی")]
        [Key]
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; NotifyPropertyChanged(nameof(ProductId)); }
        }

        [Print(Title = "نام محصول")]
        [Search(Title = "نام محصول", Description = "بخشی از نام محصول را وارد کنید", DefaultValue = "", SearchControlEnum = SearchControlType.TextBox)]
        [Required(ErrorMessage = "نام محصول نمی تواند خالی باشد")]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyPropertyChanged(nameof(Name)); }
        }

        [Print(Title = "موجودی")]
        [Required]
        public double Count
        {
            get { return _count; }
            set { _count = value; NotifyPropertyChanged(nameof(Count)); }
        }

        [NotMapped]
        public double FakeCount
        {
            get { return _fakeCount; }
            set { _fakeCount = value; NotifyPropertyChanged(nameof(FakeCount)); }
        }

        [Required]
        [Range(1, short.MaxValue, ErrorMessage = "یکی از واحدهای شمارش را انتخاب کنید")]
        public short UnitId
        {
            get { return _unitId; }
            set { _unitId = value; NotifyPropertyChanged(nameof(UnitId)); }
        }

        [ForeignKey(nameof(UnitId))]
        public Unit Unit
        {
            get { return _unit; }
            set { _unit = value; NotifyPropertyChanged(nameof(Unit)); }
        }

        [Required(AllowEmptyStrings = true)]
        [Search(Title = "شناسه یکتا", Description = "بخشی از شناسه یکتا را وارد کنید", DefaultValue = "", SearchControlEnum = SearchControlType.TextBox)]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string UniqueIdentifier
        {
            get { return _uniqueIdentifier; }
            set { _uniqueIdentifier = value; NotifyPropertyChanged(nameof(UniqueIdentifier)); }
        }

        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.VERY_LARGE_STRING_LENGTH)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; NotifyPropertyChanged(nameof(Comment)); }
        }

        [Required]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; NotifyPropertyChanged(nameof(IsDeleted)); }
        }

        public Product()
        {
            UniqueIdentifier = Comment = "";
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
