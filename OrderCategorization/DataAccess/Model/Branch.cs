﻿using OrderCategorization.Asset;
using OrderCategorization.Asset.Attributes;
using System.ComponentModel.DataAnnotations;

namespace OrderCategorization.DataAccess.Model
{
    [Model(HasTransaction = false, SingleEntityTitle = "شعبه", MultipleEntitiesTitle = "شعبه ها", KeyPropertyName = nameof(BranchId), TitlePropertyName = nameof(Title))]
    public class Branch : Model
    {
        private short _branchId;
        private string _title;
        private string _comment;

        [Key]
        public short BranchId
        {
            get { return _branchId; }
            set { _branchId = value; NotifyPropertyChanged(nameof(BranchId)); }
        }

        [Required(ErrorMessage = "عنوان شعبه نمی تواند خالی باشد")]
        [MaxLength(Constants.MEDIUM_STRING_LENGTH)]
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        [Required(AllowEmptyStrings = true)]
        [MaxLength(Constants.VERY_LARGE_STRING_LENGTH)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; NotifyPropertyChanged(nameof(Comment)); }
        }

        public Branch()
        {
            Comment = "";
        }
    }
}
